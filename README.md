# Presence

## Synposis

This project offers 3 components to be used for a physical presence log of 
people using «Legic Prime» badges. The functionality of 
the 3 components are built on top of industry standard hard and 
software solutions:

- RaspberryPi based RFID Reader embedded system (Python3 on ARM Linux)
- Java JAX-RS ([JSR 311][1]) Backend Service
  - Angular 12 Web based GUI for viewing/extracting log data

[1]: https://jcp.org/en/jsr/detail?id=311

## Architecture

```plantuml
@startuml

package "Backend System (Java 11+)" {
  database SQLite3
  
  [System Service\nJAX-RS] - Network: REST
  [System Service\nJAX-RS] -down- LDAP: "Badge ID\nLookup"
  [GUI\nAngular 12] <--> [System Service\nJAX-RS]
  SQLite3 <-- [System Service\nJAX-RS]
}

LDAP - [Identity\nManagement\nSystem]

package "Embedded System (Raspi3)" {
  database offlinedb
  [Python3 Service] - USB
  [Python3 Service] - Network: REST
  USB - [Badge Reader\nRFIDeas pxProx]
  [Python3 Service] <-down-> offlinedb
}
@enduml
```

## Components

The whole system is devided into 3 software and 2 hardware components (RasPi3 + pcProx RFID Reader). 
However, effectively only 2 software components are being packaged since the GUI is 
part fo the backend distribution.

### Embedded System

|  |  |
| -- | -- |
| Distribution | `piprox-VERSION.deb` (Debian Package) |
| Source | [Python 3 Sources][2] |
| Documentation | [README.md](client/README.md), [pcProx Protocol](client/usr/share/piprox/protocol.md), [pcProx lib](client/usr/share/piprox/README-pcprox.md), [Hardware Disasembly](client/usr/share/piprox/disassembly.md) |

### System Service

|  |  |
| -- | -- |
| Distribution | `presence-VERSION.jar` self contained Java Executable |
| Source | [Java Sources][3] |
| Documentation | [Service installation](backend/service/README.md), [Development Setup](backend/README.md) |

### GUI

|  |  |
| -- | -- |
| Distribution | none (is in the embedded system's `presence-VERSION.jar`) |
| Source | [TypeScript Sources][4] |
| Documentation | [Development Setup](backend/gui/README.md) | 

[2]: client/
[3]: backend/service/
[4]: backend/gui/

## Building

### Testing

Testing requires a working ldap isntance, use `container/slapd.sh` to start an docker
instance with slapd, a custom schema and test data on port `10389`. This instance is 
needed by unit and integration tests.

### Building the backend

Compile and deploy the gui, if there are any changes:

**1st** build the datamodel and REST Service for angular and Jax-RS:

```bash
cd backend
npm run generate:api # generates backend/gui data model and REST service
```

**2nd** compile the gui

```bash
cd backend/gui
npm run build # wil ldeploy to ../service/src/main/webapp/ui/
```

**3rd** compile backend jar

```bash
cd ../service
# skip unittest, no docker required
mvn mvn package assembly:single -DskipTests=true 
```

Test code

You need to start `slapd.sh` in the `container/` folder for tests to work. 
This requires Docker to be installed.

```bash
cd backend/service
mvn clean compile test verify
```

Build Development war for debugging

```bash
cd backend/service
mvn clean compile package
```

The new war can be found in `target/presence.war`

Build deployment package

```bash
cd backend/service
mvn clean compile package assembly:single 
```

The new executable jar can be found in `target/presence-VERSION-jar-with-dependencies.jar`.

### Building the client

see [client/README.md](client/README.md)

## Backend Database

Collected data is stored in an sqlite3 Database on the system where the service is installed.
Where the database is kept can be configured (default is `presence.sqlite3` in the same folder as 
`presence-VERSION.jar`).

### Schema

*TBD*


## Public API

This is a high level description of the REST interface. For detail specification see
[Opanepi 3 Specification][11]. If the backend service is started and the configuration value
`enable_swagger_ui` then you may inspect the current REST implementation interactively at `http://[servername]:8090/swagger-ui/`. It is recommended to disable this in a production environment (off by default).

[11]: backend/service/src/main/webapp/openapi.yaml

### POST /stamp

This endpoint is used by the embedded system to transmit a new card and timestamp.
The Client shall transmit new data in the post body as follows:

```json
{
    "uid": "DEADBEEF", // binary data in hex as string, this is the badge id
    "location": "system-123.example.com", // host-name of the device
    "stamp": "2017-07-21T17:32:28Z" // RFC 3339 Date when badge was seen
}
```

The Response may be of type `StampResult` or `BackendError`:

#### success `StampResult`, status code `200`:

```json
{
    "id": 1234, // unique db id of newly inserted record
    "time": "2017-07-21T17:32:28Z" // RFC 3339 Date
}
```

#### error `BackendError`, status code `4xx`/`5xx`:

```json
{
    "code": 123, // int internal error code
    "message": "This or that happend, sadface!", // string error message
    "ts": "2017-07-21T17:32:28Z" // RFC 3339 Date
}
```

### GET /ping

This interface is meant to do a health-check on the service. It will return a resonse 
of type `pong` which looks as follows:

#### success `Pong`, status code `200`:

```json
{
    "hostname": "servicehost.example.com", // string server system's name
    "time": "2017-07-21T17:32:28Z" // RFC 3339 Date
}
```

Any other response has to be treated as system failure.

## Requirements

- [ ] structures and gui for multiple clients
  - [x] propper room api
  - [ ] event api
    - [ ] event / room relation
  - [ ] client <-> event relation

```plantuml
@startuml

class presence {
  - int id AUTOINCREMENT
  + date stamp
  + string uid
  + string location
  + string person
  + string samaccountname
}

class room {
  - int id AUTOINCREMENT
  + string device
  + string room
}

class event {
  - int id AUTOINCREMENT
  + string name
  + time start
  + time end
}

struct client {
  - int id AUTOINCREMENT
  + string name
}

class event2room {
  - room_id
  - event_id
}

struct client2event {
  - room_id
  - event_id
}

room "1" *-- "∞" event2room
event "1" *-- "∞" event2room

client "1" *-- "∞" client2event
event "1" *-- "∞" client2event

presence "location" *-- "device" room : >

@enduml
```
