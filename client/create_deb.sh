#!/usr/bin/env bash
# create a debian package
# https://stackoverflow.com/questions/7110604/is-there-a-standard-way-to-create-debian-packages-for-distributing-python-progra

# find metadata for this package
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_DIR"
VERSION=$(grep Version deb/control | cut -d' ' -f2)
PACKAGE=$(grep Package deb/control | cut -d' ' -f2)
PKG_DIR="${PACKAGE}-${VERSION}"

# remove old package files
sudo rm -rf "$PKG_DIR" >/dev/null 2>&1

# create directory structure, $PKG_DIR is the root directory of the target system
mkdir -p $PKG_DIR/DEBIAN
mkdir -p $PKG_DIR/usr
mkdir -p $PKG_DIR/var/piprox
mkdir -p $PKG_DIR/var/log
mkdir -p $PKG_DIR/etc/

# copy files of this package
cp -r usr/* $PKG_DIR/usr
cp -r etc/* $PKG_DIR/etc
cp deb/control $PKG_DIR/DEBIAN
cp deb/piprox.postinst $PKG_DIR/DEBIAN/postinst

# fix directory references in files, make paths absolute
sed -i 's,/home/pi/pi-prox/,/,gm' $PKG_DIR/etc/udev/rules.d/60-piprox.rules
sed -i 's,/home/pi/pi-prox/,/,gm' $PKG_DIR/etc/systemd/system/piprox.service

# setup file permissions
sudo chown root:root -R $PKG_DIR/
sudo chmod 0755 $PKG_DIR/usr/bin/*

# get rid of temp files
sudo find $PKG_DIR -iname __pycache__ -exec rm -rf {} \; >/dev/null 2>&1

# build debian package
sudo dpkg -b $PKG_DIR/