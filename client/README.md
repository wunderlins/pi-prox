# waveid pcProx rfid reader service

This repo contains a set of scripts to read pxProx RFID readers under arm linux 
(tested on raspi 3+). The manufacturer only provides binaries/drives for Windows. 
This is no official Software of RFIDeas, no warranties, use at your own risk, 
you've been warned, etc. etc.

## Operations

The `piproxsrc.py` script can be run in the background. It is managed as 
a system service by systemd (see `piprox.service`). Udev ensures that an
hot-plugged in card-read will be bound to the service.

The service itself writes log files to `logs/*.log*` and can be configured 
with `pyporix.ini`. The most important configurations are in `[events]`.

## Event handlers

Before running an event handling script, the following configuration values from /etc/piprox.ini` are exposed to the script as environment variables:

- `backend_host_name`: `$HOST`
- `backend_hort`: `$PORT`

Three scripts can be configured:

- `transmit`: this script is fired whenever a new card is detected. It shall return `0` on success and any other exit code on error. The detected card's UID is provided to the script as environment variable `$CARD_UID` and contains the binary data (4 bytes) encoded as hex withotut spaces. You may configure if you'd like the data as little or big endian by setting `[device].endianness` to `BE` or `LE`.
- `startup`: is run once the configuration file and command line parameters have been parsed
- `shutdown`: if we are not killed, the shutdown script will be executed before exiting. The exit code will be available as `EXIT_CODE` environment variable to this script (see `ExitCodes` enum).

## development environment installation

```bash
# as user pi
cd # to home directory
git clone https://gitlab.com/wunderlins/presence.git
ln -s presence/client pi-prox
cd pi-prox

# dependencies / permissions
sudo -i # run everything as root

apt install -y sqlite3

addgroup piprox
adduser pi piprox

# python service
pip install hidapi # dependecy

install -o0 -g0 -m0644 etc/udev/rules.d/60-piprox.rules /etc/udev/rules.d/
udevadm control --reload-rules # reload udev

install -o0 -g0 -m0644 etc/systemd/system/piprox.service /etc/systemd/system/
systemctl daemon-reload
systemctl start piprox.service # start service
```

**CAVE**: restart session (logout / login) to make group avaialbe, verify:

```bash
# check if group exists
groups | grep piprox && echo "Success" || echo "Nope mate!"

# check if service is running
systemctl status piprox

# check the logs for errors
journalctl --follow -u piprox
```

If the reader has been plugged in on the USB Port, disconnect and 
re-connect now (udev rules have changed).

## build deb

The script `create_deb.sh` will build the required debian package. If you want to 
build and deploy a new version, then you'll have to considre the following steps 
before building the deb package:

1. bump the version (`deb/control`) or the user will not be uble to upgrade existing installtions without `--force`. The Debian version number shall use the format `M.m.P` (`M`ajor, `m`inor, `P`atch).
2. If you have added resources, you will have to add them in the script in the «# copy files» section. This is fairly simple, `$PKG_DIR/` will be the root directory of the target system (`/`), you will have to create your directroy structure from there. 

# BIN -> DEC Puzzle

This is part of the revers engineering of the contained id. It seesm, 
that the first 4 bytes of the RFID's chip is the UID or so called 
Mifare card id.

It seems, that this is our primary token and addition data stored on 
the chip (up to 10 or 12 bytes) are unused or at least irellevant 
for this application.

my card numbers (first 4 bytes):
- BIN: d2 90 e4 40 / 40 e4 90 d2 # depending on endianness
- DEC: (According to imprivata) 114:18537

```
HEX Mifare UID
D2     1101 0010
90     1001 0000
E4     1110 0100
40     0100 0000

DEC Imprivata ID
114    0111 0010
18537  0100 1000 0110 1001
```

There is probabaly no relation btween the card's binary UID and the imprivata ID.


