#!/usr/bin/env bash
# 
# when this sicript is run, al lfaild stamps from today are 
# resubmitted to the service
# 
# CREATE TABLE log (
#                id INTEGER PRIMARY KEY AUTOINCREMENT,
#                tst TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
#                badge_id TEXT,
#                ret NUMBER,
#                ret_string TEXT
#            );
#

function usage() {
	1>&2 cat << EOT
Find unsubmitted stamps from one day an re-submit them.

usage: $0 [--dry-run] <days_ago>

	--dry-run   will parse the database and find unsubmitted logs.
	days_ago    find unsubmitted stamps N days ago (1 is yesterday, etc.)

FILES:

	/etc/piprox.ini                  
		using host/port configuration from this file
	/var/piprox/transaction_log.db   
		log of all reader activities

2022-04-13 Simon Wunderlin
EOT
}

function check_exe() {
	which "$1" >/dev/null 2>&1
	if [[ "$?" != "0" ]]; then
		1>&2 echo "Executable $1 not found. Please install and make sure it is in \$PATH"
		exit 2
	fi
}

function reverse_uid() {
	echo ${1:6:2}${1:4:2}${1:2:2}${1:0:2}
}

if [[ -z "$1" ]]; then
	usage
	exit 1;
fi

# check for required command line utilities
check_exe jq
check_exe sqlite3
check_exe wget
check_exe cut
check_exe awk

# find config
HOST=$(awk -F'=' '/^backend_host_name/ {gsub(/ /, "", $2); print $2}' /etc/piprox.ini)
PORT=$(awk -F'=' '/^backend_port/ {gsub(/ /, "", $2); print $2}' /etc/piprox.ini)
#HOST=wus-desktop6.dyn.uhbs.ch # debug

if [[ -z "$HOST" || -z "$PORT" ]]; then
	1>&2 echo "Config data not found in /etc/piprox.ini"
	1>&2echo "HOST: $host"
	1>&2 echo "PORT: $PORT"
	exit 3;
fi

dry_run=0
if [[ "$1" == "--dry-run" ]]; then
	dry_run=1
	shift
fi

if [[ "$1" == "-h" || "$1" == "--help" ]]; then
	usage
	exit 0;
fi

days_ago=1
if [[ ! -z "$1" ]]; then
	days_ago=$1
fi

date=$(date --date="now -$days_ago day" "+%Y-%m-%d")
#echo "$date"
location=$(hostname)
sql="select id, strftime('%Y-%m-%dT%H:%M:%S+02', tst), badge_id from log where tst like '$date%' and ret_string == ''"
#echo "$sql"
recs=$(sqlite3 /var/piprox/transaction_log.db "$sql")

if [[ "$?" -ne "0" ]]; then
	1>&2 echo "Failed to query databse."
	exit 4
fi

IFS="
"

if [[ "$dry_run" -ne "1" ]]; then
	echo "Sending data ..."
fi

for rec in $recs; do
	id=$(echo "$rec" | cut -d'|' -f1)
	time=$(echo "$rec" | cut -d'|' -f2)
	badge=$(echo "$rec" | cut -d'|' -f3)
	badge=$(reverse_uid $badge)

	export url=http://${HOST}:${PORT}/api/stamp
	export payload="{\"uid\": \"$badge\", \"location\": \"$location\", \"stamp\": \"$time\"}"

	echo "$payload" | jq
	
	if [[ "$dry_run" -ne "1" ]]; then
		# submit
		echo $url
		RESULT=$(
		    wget -timeout=3 \
		        --post-data "$payload" \
		        --header "Content-type: application/json; charset=utf-8" \
		        -qO- "$url"
		)
		ret=$?
		# log response
		echo "RESULT: $ret $RESULT"

		# check if the backend service could find a name for this badge
		name=$(echo $RESULT | jq '.name')
		
		# update database
		sql="update log set ret=$ret, ret_string='$RESULT' WHERE id=$id"
		#echo "$sql"
		sqlite3 /var/piprox/transaction_log.db "$sql"
		#break
	fi
done

