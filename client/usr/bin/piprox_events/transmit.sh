#!/usr/bin/env bash
set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
logfile=$SCRIPT_DIR/../../var/piprox/piprox_transmit.log

# this is an example transmit event handler which sends the badge number to 
# a web service.

# a random toilet on Henry's Post Test Server
#url="https://ptsv2.com/t/ftqx2-1642001722/post"
#payload="{\"uid\":\"${BADGE_UID}\"}"
url=http://${HOST}:${PORT}/api/stamp
payload="{\"uid\":\"${BADGE_UID}\", \"location\": \"$(hostname)\", \"stamp\": \"$(date --iso-8601=seconds)\"}"

RESULT=$(
    wget -timeout=3 \
        --post-data "${payload}" \
        --header "Content-type: application/json; charset=utf-8" \
        -qO- "$url"
)
ret=$?

# log response
echo "$RESULT"

# check if the backend service could find a name for this badge
#name=$(echo $RESULT | jq '.name')

# if we failed to resolve the badge's name
#if [[ "$name" == "null" ]]; then 
#    exit 1; # could not resolve the person's badge UID
#fi

# all fine
exit $ret;
