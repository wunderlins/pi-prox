#!/usr/bin/env bash

# this is an example of a transmit event script. It reutnrs errors randomly
# by using an exit code other than 0. this can be used to 
# test the led behaviour on the badge scanner device.
#
# 

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
logfile=$SCRIPT_DIR/../../var/log/piprox_example_transmit.log


dt=$(date +"%Y-%m-%d %H:%M:%S")
echo "${dt} ${BADGE_UID}" >> $logfile

# random exit code
ret=$((0 + $RANDOM % 2))
exit $ret