#!/usr/bin/env bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

sqlite3 -header -csv $SCRIPT_DIR/../../var/piprox/transaction_log.db "select * from log" > transaction_log.csv

echo -e "Exported all records to transaction_log.csv"
echo -e "" 
echo -e "Field delimiter: ','"
echo -e "Record delimiter: '\\\\n'"
echo -e "Quote character: '\"'"