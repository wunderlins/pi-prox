#!/usr/bin/env bash

## start slapd container with basic test data
##

LDAP_NAME="test_presence_slapd"
LDAP_DOMAIN="mycorp.com"
LDAP_ORGANISATION="ACME Co. Ltd."
LDAP_ROOTPASS="s3kre33t"
LDAP_PORT="10389"
#LDAPS_PORT="10636"

docker container stop "$LDAP_NAME"
docker container rm "$LDAP_NAME"

# https://github.com/osixia/docker-openldap
#	-v $PWD/data/ldap:/var/lib/ldap \
#	-v $PWD/data/slapd.d:/etc/ldap/slapd.d \
docker run \
	--env LDAP_ORGANISATION="$LDAP_ORGANISATION" \
	--env LDAP_DOMAIN="$LDAP_DOMAIN" \
	--env LDAP_ADMIN_PASSWORD="$LDAP_ROOTPASS" \
	-v $PWD/test_data:/root/data \
	-p $LDAP_PORT:389 \
	--name $LDAP_NAME \
	--detach osixia/openldap:1.5.0
	
# wait for socket to become available
echo "Waiting for slapd to become available ..."
#while nc -z localhost $LDAP_PORT ; [ $? -ne 0 ];do
#    sleep 1;
#done
sleep 3 # give the service some time to settle

# now load our schema and test data
docker exec $LDAP_NAME ldapadd -Y EXTERNAL -H ldapi:/// -f /root/data/badge_schema.ldif
docker exec $LDAP_NAME ldapadd -Y EXTERNAL -H ldapi:/// -f /root/data/people.ldif

LDAP_USER=cn=admin,dc=$(echo $LDAP_DOMAIN | sed -e 's/\./,dc=/g')
echo "DOCKER $LDAP_NAME"
echo "PORT   $LDAP_PORT"
echo "USER   $LDAP_USER"
echo "PASS   $LDAP_ROOTPASS"
echo ""
echo "Add data with the following commands"
echo "From this machine:"
echo "\$ ldapadd -h localhost -p $LDAP_PORT -c -x -D $LDAP_USER -w \"$LDAP_ROOTPASS\" -f data.ldif"
echo "From within the container, will be run as root:"
echo "\$ docker exec $LDAP_NAME ldapsearch -x -H ldap://localhost -D \"$LDAP_USER\" -w $LDAP_ROOTPASS"
