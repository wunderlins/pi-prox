# Docker Services for unit testing

## run docker service

```bash
$ ./slapd.sh 
test_presence_slapd
test_presence_slapd
63eb4cc5ed6142c7ee577e1333fa874208b58966fe28bdd112368ecef557c98a
Waiting for slapd to become available ...
SASL/EXTERNAL authentication started
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
SASL SSF: 0
adding new entry "cn=badge,cn=schema,cn=config"

SASL/EXTERNAL authentication started
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
SASL SSF: 0
adding new entry "ou=people,dc=mycorp,dc=com"

adding new entry "cn=persono,ou=people,dc=mycorp,dc=com"

adding new entry "cn=persont,ou=people,dc=mycorp,dc=com"

adding new entry "cn=personr,ou=people,dc=mycorp,dc=com"

DOCKER test_presence_slapd
PORT   10389/10636
USER   cn=admin,dc=mycorp,dc=com
PASS   s3kre33t

Add data with the following commands
From this machine:
$ ldapadd -h localhost -p 10389 -c -x -D cn=admin,dc=mycorp,dc=com -w "s3kre33t" -f data.ldif
From within the container, will be run as root:
$ docker exec test_presence_slapd ldapsearch -x -H ldap://localhost -D "cn=admin,dc=mycorp,dc=com" -w s3kre33t
```

now there is `slapd` an instance running at the above reported port, you can use the 
parameters provided to test if the instance is running, schema loaded and polulated with test data:

```bash
ldapsearch -h localhost -p 10389 -c -x -D cn=admin,dc=mycorp,dc=com -w "s3kre33t" \
  -b ou=people,dc=mycorp,dc=com '(objectClass=BadgeObject)'
```

this query should return 3 objects:
```bash
...
# numResponses: 4
# numEntries: 3
```

## install docker

### Debian 11

[Manual for Debian](https://docs.docker.com/engine/install/debian/)

```bash
sudo -i # become root
apt update
# remove old installations
apt remove docker docker-engine docker.io containerd runc
# setup dependencies
apt install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
# setup source
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
# install docker
apt update
apt install docker-ce docker-ce-cli containerd.io
# add your user to the docker group
addgroup <username> docker
# you need to log in again if you are using a graphical shell
```

[ldap add schema](https://www.gurkengewuerz.de/openldap-neue-schema-hinzufuegen/?cookie-state-change=1643304760409)


### OSX

```bash
sudo port install docker
```

## notes

add schema to openldap instance in docker

```bash
# inside the container
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/slapd.d/badge_schema.ldif
# outside the container
docker exec <instance> ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/slapd.d/badge_schema.ldif
```
