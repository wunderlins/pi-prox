-- update presence table
alter table presence add location_id INTEGER;
alter table presence add device TEXT;
alter table presence add event TEXT;
alter table presence add event_id INTEGER;
CREATE TABLE room2 (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    device TEXT  UNIQUE,
    room TEXT
);

-- add primary key to room table, we need to copy it
insert into room2 (device, room) select device, room from room;
DROP TABLE room;
ALTER TABLE `room2` RENAME TO `room`;

-- create new tables
CREATE TABLE event (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    start_time INTEGER, -- time format: `[H]HMM`
    end_time INTEGER,    -- time format: `[H]HMM`
    dow INTEGER, -- day of week 0 (mon) - 6 (Sun)
    deleted INTEGER DEFAULT 0 -- 0: false, 1: true
);
CREATE TABLE event2room (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    room_id INTEGER,
    event_id INTEGER
);


