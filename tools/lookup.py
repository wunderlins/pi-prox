#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
read csv file and convert badge numbers into Names

This progeamm expects a csv file with the following format:
YYYY-mm-dd HH:MM:SS;badge_id

the badge_id can be looked up in an IDM directory via ldap

"""

import sys, os, csv
import ldap
from pprint import pprint

name_cache = {}
lconn = None

def ldapconn():
	ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
	l = ldap.initialize('ldap://ictidmldplp03.uhbs.ch:389')
	binddn = "cn=ldapread,o=services"
	pw = "idmreadldap"
	l.protocol_version = ldap.VERSION3
	l.simple_bind_s(binddn, pw) 

	return l

def dir_resolve(id):
	global name_cache
	
	# use cached data if available
	rec = [None, None]
	try:
		rec = name_cache[id]
		return rec
	except: pass
	
	# ldap setup
	basedn = "ou=PERSON,ou=AUTHENTICATION,o=AI"
	searchFilter = "(cn={})".format(id)
	#print(searchFilter)
	searchAttribute = ["cn", "AIAUTHGUID","AIUSBBadgeNumber", "AIUniquePersonID", "mail", "fullName"]
	#searchAttribute = None
	searchScope = ldap.SCOPE_SUBTREE
	
	# search 
	ldap_result_id = lconn.search(basedn, searchScope, searchFilter, searchAttribute)
	
	while 1:
		result_type, result_data = lconn.result(ldap_result_id, 0)
		#pprint((result_type, result_data))
		if (result_data == []):
			break
		else:
			## if you are expecting multiple results you can append them
			## otherwise you can just wait until the initial result and break out
			#if result_type == ldap.RES_SEARCH_ENTRY:
			#	result_set.append(result_data)
			cn = result_data[0][0]
			#print(result_data[0][0])
			
			# cache name for faster lookup
			res = [
				"", #result_data[0][1]["cn"][0], 
				"", #result_data[0][1]["AIAUTHGUID"][0],
				"", #result_data[0][1]["AIUSBBadgeNumber"][0], 
				"", #result_data[0][1]["AIUniquePersonID"][0],
				"", #result_data[0][1]["mail"][0], 
				"", #result_data[0][1]["fullName"][0]
			]
			
			try: 
				res[0] = result_data[0][1]["cn"][0]; 
			except: pass
			try: 
				res[1] = result_data[0][1]["AIAUTHGUID"][0]; 
			except: pass
			try: 
				res[2] = result_data[0][1]["AIUSBBadgeNumber"][0]; 
			except: pass
			try: 
				res[3] = result_data[0][1]["AIUniquePersonID"][0]; 
			except: pass
			try: 
				res[4] = result_data[0][1]["mail"][0]; 
			except: pass
			try: 
				res[5] = result_data[0][1]["fullName"][0]; 
			except: pass
			
			#pprint(res)
			name_cache[id] = res

			return res
	
	return [None, None, None, None, None, None]	

def hex_guid_to_str(guid):
	# in:  DBABA5DBD3EDAD4964AFDBABA5DBD3ED
	# out: 4d96567f-b15e-4b6b-bb84-8289cf0a4026
	return guid[0:7].decode("utf-8") + "-" + guid[8:11].decode("utf-8") + "-" + guid[12:15].decode("utf-8") + "-" + guid[16:19].decode("utf-8") + "-" + guid[20:].decode("utf-8")

def lookup(filename=None):
	
	# incput validation
	if filename == None:
		raise ValueError("filename must be provided")
	if not os.path.isfile(filename):
		raise ValueError("File '{}' does not exist".format(filename))
	
	# try to open
	fp = None
	try:
		fp = open(filename)
	except IOError:
		fp.close()
		raise ValueError("File '{}' cannot be opened".format(filename))
	
	# process content
	csv_reader = csv.reader(fp, delimiter=',')
	next(csv_reader); next(csv_reader) # skip 2 lines

	out_name = filename
	fpo = open(out_name.split(".")[0] + ".xls", 'w+b')
	fpo.write(bytearray([0xEF, 0xBB, 0xBF]))
	fpo.write(b"LegicUID;user;guid;nedap;PersonID;mail;FullName\r\n");
	
	for row in csv_reader:
		account = None
		try:
			account = row[0].replace("@ms.uhbs.ch", "")
		except: pass
		
		# no id, ignore
		if not account: 
			continue 
		
		# find name for badge number in idm
		rec = dir_resolve(account)
		if rec == None: rec = [None, None, None, None, None, None]
		#print(rec)
		
		n = rec[0]
		#if rec[0] != None:
		#	n = rec[0].decode("utf-8").encode("latin-1")
		
		print("    " + str([row[0], row[1], n, rec[1]]))
		encoding = "UTF8"
		g = ""
		if rec[1] != None:
			g = hex_guid_to_str(rec[1])
		line = str(row[1]) + ";" + str(rec[0]) + ";" + str(g) + ";" + str(rec[2]) + ";" + str(rec[3]) + ";"  + str(rec[4]) + ";" + rec[5].decode("utf8")  + "\r\n"
		fpo.write(bytes(line.replace("b'", '').replace("'", ""), 'utf-8'))
	
if __name__ == '__main__':
	lconn = ldapconn()
	lookup(sys.argv[1])
	

