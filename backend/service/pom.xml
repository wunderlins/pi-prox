<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <groupId>net.wunderlin</groupId>
    <artifactId>presence</artifactId>
    <packaging>war</packaging>
    <version>0.5.1</version>
    <name>presence</name>

    <build>
        <finalName>presence</finalName>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <inherited>true</inherited>
                <configuration>
                    <source>${maven.compiler.source}</source>
                    <target>${maven.compiler.target}</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <version>1.9.1</version>
                <executions>
                    <execution>
                    <id>add-source</id>
                    <phase>generate-sources</phase>
                    <goals>
                        <goal>add-source</goal>
                    </goals>
                    <configuration>
                        <sources>
                        <source>src/gen/java</source>
                        </sources>
                    </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.22.0</version>
                <configuration>
                    <includes>
                        <include>**/Test*.java</include>
                    </includes>
                    <excludes>
                        <!-- exclude ldap test when you have no ldap connection -->
                        <!--exclude>TestLdap</exclude-->
                    </excludes>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>2.22.0</version>
                <configuration>
                    <includes>
                        <include>**/ITest*.java</include>
                    </includes>
                    <excludes>
                        <!--exclude>some test to exclude here</exclude-->
                    </excludes>
                </configuration>
                <executions>
                    <execution>
                        <id>integration-test</id>
                        <goals>
                            <goal>integration-test</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>verify</id>
                        <goals>
                            <goal>verify</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- swagger-ui-->
            <plugin>
                <!-- Download Swagger UI webjar. -->
                <artifactId>maven-dependency-plugin</artifactId>
                <version>${maven-dependency-plugin.version}</version>
                <executions>
                    <execution>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>unpack</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>org.webjars</groupId>
                                    <artifactId>swagger-ui</artifactId>
                                    <version>${swagger-ui.version}</version>
                                </artifactItem>
                            </artifactItems>
                            <outputDirectory>${project.build.directory}/swagger-ui</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <!-- Add Swagger UI resources to the war file. -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>${maven-war-plugin.version}</version>
                <configuration>
                    <webResources combine.children="append">
                        <resource>
                            <directory>${project.build.directory}/swagger-ui/META-INF/resources/webjars/swagger-ui/${swagger-ui.version}</directory>
                            <includes>
                                <include>**/*.*</include>
                            </includes>
                            <targetPath>swagger-ui</targetPath>
                        </resource>
                    </webResources>
                </configuration>
            </plugin>
            <plugin>
                <!-- Replace the OpenAPI specification example URL with the local one. -->
                <groupId>com.google.code.maven-replacer-plugin</groupId>
                <artifactId>replacer</artifactId>
                <version>${replacer.version}</version>
                <executions>
                    <execution>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>replace</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <file>${project.build.directory}/swagger-ui/META-INF/resources/webjars/swagger-ui/${swagger-ui.version}/index.html</file>
                    <replacements>
                        <replacement>
                            <token>http://petstore.swagger.io/v2/swagger.json</token>
                            <value>../openapi.yaml</value>
                        </replacement>
                    </replacements>
                </configuration>
            </plugin>
            <!-- END Swagger ui -->

            <!-- ueber jar depeendencies -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                    <finalName>${project.name}-${project.version}</finalName>
                    <archive>
                        <manifest>
                            <mainClass>${main.class}</mainClass>
                            <addDefaultImplementationEntries>
                            true
                            </addDefaultImplementationEntries>
                        </manifest>
                    </archive>
                </configuration>
                <executions>
                    <execution>
                        <configuration>
                            <appendAssemblyId>false</appendAssemblyId>
                        </configuration>
                        <phase>never</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- end ueber jar depeendencies -->

            <!-- deploy openapi.yaml file web root for swagger-ui -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>${maven-resources-plugin.version}</version>
                <executions>
                    <!-- copy openapi.yaml for war deployment -->
                    <execution>
                        <id>copy-openapi-yaml</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <overwrite>true</overwrite>
                            <outputDirectory>${project.build.directory}/${project.artifactId}-${project.version}/</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>${project.basedir}</directory>
                                    <includes>
                                        <include>openapi.yaml</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                    <!-- copy openapi.yaml for uber jar deployment -->
                    <execution>
                        <id>copy-openapi-yaml-to-ui</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <overwrite>true</overwrite>
                            <outputDirectory>${project.build.directory}/${project.artifactId}/ui/</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>${project.basedir}/src/main/webapp/</directory>
                                    <includes>
                                        <include>openapi.yaml</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <?m2e execute onConfiguration?>
                        <id>antrun-compile</id>
                        <phase>test-compile</phase>
                        <configuration>
                            <target>
                                <taskdef resource="net/sf/antcontrib/antlib.xml" classpathref="maven.dependency.classpath" />
                                <copy file="${project.basedir}/src/main/resources/application.properties" tofile="${project.build.directory}/application.properties" overwrite="true"/>
                                <if>
                                    <available file="${project.basedir}/src/main/resources/application.properties.private"/>
                                    <then>
                                        <!-- Do something with it -->
                                        <copy file="${project.basedir}/src/main/resources/application.properties.private" tofile="${project.build.directory}/application.properties" overwrite="true"/>
                                        <copy file="${project.basedir}/src/main/resources/application.properties.private" tofile="${project.build.directory}/classes/application.properties" overwrite="true"/>
                                        <copy file="${project.basedir}/src/main/resources/application.properties.private" tofile="${project.build.directory}/${project.artifactId}/application.properties" overwrite="true"/>
                                    </then>
                                    <else>
                                        <!--copy file="${project.basedir}/src/main/resources/application.properties" tofile="${project.build.directory}/application.properties" overwrite="true"/-->
                                        <!--copy file="${project.basedir}/src/main/resources/application.properties.dist" tofile="${project.build.directory}/classes/application.properties" overwrite="true"/-->
                                        <!--copy file="${project.basedir}/src/main/resources/application.properties.dist" tofile="${project.build.directory}/${project.artifactId}/application.properties" overwrite="true"/-->
                                    </else>
                                </if>
                                <!-- ui for war-->
                                <copy todir="${project.build.directory}/${project.artifactId}">
                                    <fileset dir="${project.basedir}/src/main/webapp/ui"/>
                                </copy>
                                <delete file="${project.build.directory}/classes/application.properties.dist" failonerror="false"/>
                                <delete file="${project.build.directory}/classes/application.properties.private" failonerror="false"/>
                                <delete file="${project.build.directory}/classes/application.properties.private_" failonerror="false"/>
                                <!--delete dir="${project.build.directory}/${project.artifactId}/ui" failonerror="false" deleteonexit="true"/-->
                            </target>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>antrun-package</id>
                        <phase>prepare-package</phase>
                        <configuration>
                            <target>
                                <taskdef resource="net/sf/antcontrib/antlib.xml" classpathref="maven.dependency.classpath" />
                                <copy file="${project.basedir}/src/main/webapp/openapi.yaml" tofile="${project.build.directory}/classes/ui/openapi.yaml" overwrite="true"/>
                                <copy todir="${project.build.directory}/classes/ui">
                                    <fileset dir="${project.basedir}/src/main/webapp/ui/" />
                                </copy>
                                <copy todir="${project.build.directory}/classes/swagger-ui">
                                    <fileset dir="${project.build.directory}/swagger-ui/META-INF/resources/webjars/swagger-ui/${swagger-ui.version}/" />
                                </copy>
                                <replace file="${project.build.directory}/classes/swagger-ui/index.html" token="http://petstore.swagger.io/v2/swagger.json" value="../openapi.yaml" />
                                <replace file="${project.build.directory}/${project.artifactId}/index.html" token="&lt;base href=&quot;/&quot;&gt;" value="&lt;base href=&quot;/presence/&quot;&gt;" />
                                <!--copy file="${project.basedir}/src/main/resources/application.properties.dist" tofile="${project.build.directory}/classes/${project.artifactId}/WEB-INF/classes/application.properties" overwrite="true"/-->
                            </target>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>ant-contrib</groupId>
                        <artifactId>ant-contrib</artifactId>
                        <version>1.0b3</version>
                        <exclusions>
                            <exclusion>
                                <groupId>ant</groupId>
                                <artifactId>ant</artifactId>
                            </exclusion>
                        </exclusions>
                    </dependency>
                    <dependency>
                        <groupId>org.apache.ant</groupId>
                        <artifactId>ant-nodeps</artifactId>
                        <version>1.8.1</version>
                    </dependency>
                </dependencies>
            </plugin>
        </plugins>
    </build>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.glassfish.jersey</groupId>
                <artifactId>jersey-bom</artifactId>
                <version>${jersey.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- https://mvnrepository.com/artifact/com.google.guava/guava -->
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>31.0.1-jre</version>
        </dependency>
        <dependency>
            <groupId>org.glassfish.jersey.containers</groupId>
            <!--artifactId>jersey-container-servlet-core</artifactId-->
            <!-- use the following artifactId if you don't need servlet 2.x compatibility -->
            <artifactId>jersey-container-servlet</artifactId>
        </dependency>
        <dependency>
            <groupId>org.glassfish.jersey.inject</groupId>
            <artifactId>jersey-hk2</artifactId>
        </dependency>

        <!--dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>${jackson-version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>${jackson-version}</version>
        </dependency-->

        <dependency>
            <groupId>com.fasterxml.jackson.datatype</groupId>
            <artifactId>jackson-datatype-jsr310</artifactId>
            <version>${jackson-version}</version>
        </dependency>
        <dependency>
          <groupId>com.fasterxml.jackson.jaxrs</groupId>
          <artifactId>jackson-jaxrs-json-provider</artifactId>
          <version>${jackson-version}</version>
        </dependency>

        <dependency>
            <groupId>org.glassfish.jersey.media</groupId>
            <artifactId>jersey-media-jaxb</artifactId>
            <version>${jersey.version}</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.glassfish.jersey.media/jersey-media-json-jackson -->
        <dependency>
            <groupId>org.glassfish.jersey.media</groupId>
            <artifactId>jersey-media-json-jackson</artifactId>
            <version>${jersey.version}</version>
        </dependency>

        <!-- uber jar -->
        <dependency>
            <groupId>org.glassfish.jersey.containers</groupId>
            <artifactId>jersey-container-grizzly2-http</artifactId>
        </dependency>
        <dependency>
            <groupId>org.glassfish.jersey.core</groupId>
            <artifactId>jersey-client</artifactId>
            <version>${jersey.version}</version>
        </dependency>


        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-jersey2-jaxrs</artifactId>
            <scope>compile</scope>
            <version>${swagger-core-version}</version>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>${servlet-api-version}</version>
        </dependency>

        <!-- manually added -->
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>${jaxb-api-version}</version>
        </dependency>
        <dependency>
            <groupId>org.glassfish.jaxb</groupId>
            <artifactId>jaxb-runtime</artifactId>
            <version>2.3.5</version>
        </dependency>

        <dependency>
            <groupId>javax.activation</groupId>
            <artifactId>activation</artifactId>
            <version>${javax.activation-version}</version>
        </dependency>

        <!-- unit tests -->
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit-version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit-version}</version>
            <scope>test</scope>
        </dependency>

        <!-- https://mvnrepository.com/artifact/com.googlecode.json-simple/json-simple -->
        <!--dependency>
            <groupId>com.googlecode.json-simple</groupId>
            <artifactId>json-simple</artifactId>
            <version>${simple-json-version}</version>
        </dependency-->

        <!-- swagger ui-->
        <dependency>
            <groupId>io.swagger.core.v3</groupId>
            <artifactId>swagger-jaxrs2</artifactId>
            <version>${swagger.version}</version>
        </dependency>
        <dependency>
            <groupId>io.swagger.core.v3</groupId>
            <artifactId>swagger-jaxrs2-servlet-initializer</artifactId>
            <version>${swagger.version}</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc -->
        <dependency>
            <groupId>org.xerial</groupId>
            <artifactId>sqlite-jdbc</artifactId>
            <version>3.36.0.3</version>
        </dependency>

        <!-- logging -->
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback-version}</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-core</artifactId>
            <version>${logback-version}</version>
            <scope>compile</scope>
        </dependency>

    </dependencies>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>

        <jersey.version>2.35</jersey.version>
        <jackson-version>2.14.1</jackson-version>
        <swagger-core-version>1.6.4</swagger-core-version>
        <servlet-api-version>2.5</servlet-api-version>
        <jaxb-api-version>2.3.1</jaxb-api-version>
        <junit-version>5.4.0</junit-version>
        <javax.activation-version>1.1.1</javax.activation-version>
        <simple-json-version>1.1</simple-json-version>

        <swagger.version>2.0.2</swagger.version>
        <swagger-ui.version>3.17.0</swagger-ui.version>
        <replacer.version>1.5.3</replacer.version>
        <maven-war-plugin.version>3.2.2</maven-war-plugin.version>
        <maven-dependency-plugin.version>3.1.1</maven-dependency-plugin.version>
        <maven-resources-plugin.version>2.4.2</maven-resources-plugin.version>

        <tomcat.version>9.0.50</tomcat.version>
        <main.class>net.wunderlin.presence.Main</main.class>
        <logback-version>1.3.0-alpha12</logback-version>
    </properties>
</project>
