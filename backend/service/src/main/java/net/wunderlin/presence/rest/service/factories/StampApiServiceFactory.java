package net.wunderlin.presence.rest.service.factories;

import net.wunderlin.presence.rest.service.StampApiService;
import net.wunderlin.presence.rest.service.impl.StampApiServiceImpl;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class StampApiServiceFactory {
    private static final StampApiService service = new StampApiServiceImpl();

    public static StampApiService getStampApi() {
        return service;
    }
}
