package net.wunderlin.presence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.wunderlin.presence.rest.model.StampRecord;

public class DaoService {
    static Logger logger = LoggerFactory.getLogger(DaoService.class);

    protected static ConfigService configService = ConfigService.getInstance();
    public static ZoneId zone = ZoneId.of(configService.properties.getProperty("time-zone"));
    public static ZoneOffset zoneOffSet = zone.getRules().getOffset(LocalDateTime.now());
    private Connection conn = null;

    /**
     * singleton instance
     */
    private static DaoService INSTANCE;

    /**
     * singleton
     * @return mock service instance
     * @throws Exception
     */
    public static DaoService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new DaoService();
            INSTANCE.open();
        }
        return INSTANCE;
    }

    public DaoService() {}

    public void open() {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            //System.out.println(e.getMessage());
            logger.error(e.getMessage());
            e.printStackTrace();
        }

        // database file
        String dbfile = getDbFileName();

        try {
            // db parameters
            String url = "jdbc:sqlite:" + dbfile;
            logger.info("Opening: " + url);
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            // if this is an empty database, create it
            String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='config'";
            Statement stmt = conn.createStatement();
            ResultSet res = stmt.executeQuery(sql);

            int size = 0;
            while (res.next()) size++;

            // new database, create schema
            if (size == 0) {
                logger.info("Empty database, creating tables");
                try (Connection conn = DriverManager.getConnection(url);
                    Statement stmt1 = conn.createStatement()) {
                    String createSql = "CREATE TABLE presence (\n" +
                    "    id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "    stamp DATE,\n" +
                    "    uid TEXT,\n" +
                    "    device TEXT,\n" +
                    "    location TEXT,\n" +
                    "    location_id INTEGER DEFAULT -1,\n" +
                    "    person TEXT,\n" +
                    "    samaccountname TEXT,\n" +
                    "    event TEXT,\n" +
                    "    event_id INTEGER DEFAULT -1\n" +
                    ")";
                    stmt1.execute(createSql);

                    createSql = "\n" +
                    "CREATE TABLE config (\n" +
                    "    key TEXT UNIQUE,\n" +
                    "    value TEXT\n" +
                    ")";
                    stmt1.execute(createSql);

                    createSql = "\n" +
                    "CREATE TABLE room (\n" +
                    "    id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "    device TEXT  UNIQUE,\n" +
                    "    room TEXT\n" +
                    ")";
                    stmt1.execute(createSql);

                    createSql = "\n" +
                    "CREATE TABLE IF NOT EXISTS \"imprivata\" (\n" +
                    "    \"LegicUID\"      TEXT,\n" +
                    "    \"sAMAccountNAme\"        TEXT,\n" +
                    "    \"GUID\"  TEXT,\n" +
                    "    \"NedapID\"       INTEGER,\n" +
                    "    \"PersonalId\"    INTEGER,\n" +
                    "    \"EMail\" TEXT,\n" +
                    "    \"FullName\"      TEXT\n" +
                    ")";
                    stmt1.execute(createSql);

                    createSql = "\n" +
                    "CREATE TABLE event (\n" +
                    "    id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "    name TEXT,\n" +
                    "    start_time INTEGER, -- time format: `[H]HMM`\n" +
                    "    end_time INTEGER,    -- time format: `[H]HMM`\n" +
                    "    dow INTEGER, -- day of week 0 (mon) - 6 (Sun)\n" +
                    "    deleted INTEGER DEFAULT 0 -- 0: false, 1: true\n" +
                    ");\n";
                    stmt1.execute(createSql);

                    createSql = "\n" +
                    "CREATE TABLE event2room (\n" +
                    "    id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "    room_id INTEGER,\n" +
                    "    event_id INTEGER\n" +
                    ");\n";
                    stmt1.execute(createSql);

                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                    logger.error("Failed to create tables");
                    logger.error(e.getMessage());
                }
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            logger.error(e.getMessage());
        } finally {
            /*
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                //logger.error(ex.getMessage());
                System.out.println(ex.getMessage());
            }
            */
        }
    }

    public StampRecord getUserName(String UID) throws SQLException {
        StampRecord ret = new StampRecord().id(-1);

        String reverseUID = "" + UID.charAt(6) + UID.charAt(7) +
                                 UID.charAt(4) + UID.charAt(5) +
                                 UID.charAt(2) + UID.charAt(3) +
                                 UID.charAt(0) + UID.charAt(1);

        String sql = "SELECT FullName, sAMAccountName FROM imprivata WHERE LegicUID = ? OR LegicUID = ?";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, UID);
        pstmt.setString(2, reverseUID);
        ResultSet rs = pstmt.executeQuery();

        int count = 0;
        while (rs.next()) {
            if (count > 0) {
                logger.warn("Found more than one Legic UID in Improvata database: " + UID);
                return ret;
            }

            ret.setId(0);
            ret.setName(rs.getString("FullName"));
            ret.setsAMAccountName(rs.getString("sAMAccountName"));
            count++;
        }

        return ret;
    }


    public String getDbFileName() {
        // database file
        String dbfile = configService.getConfig().getProperty("database-file");

        // no need to change anything for memory dbs
        if (dbfile.equals(":memory:"))
            return dbfile;

        if (!dbfile.substring(0, 1).equals("/") && // no absolute path and no windows drive letter
            !dbfile.substring(1, 2).equals(":") && // and no unc path
            !dbfile.substring(0, 2).equals("\\\\") ) {
                dbfile = ConfigService.propertiesFolder + "/" + dbfile;
        }
        return dbfile;
    }

    public void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        //if (conn == null) open();
        return conn;
    }

    public Integer findEvent(StampRecord stamp) throws Exception, SQLException {
        logger.debug("Finding event for: " + stamp.getLocation() + " " + stamp.getTime() + " " + stamp.getUid());
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        String sql = "";
        Integer ret = -1;

        // find event according to room and time
        String time_str = stamp.getTime().atZoneSameInstant(ZoneId.of("Europe/Zurich")).toLocalDateTime().format(DateTimeFormatter.ofPattern("HHmm"));

        // map day of week. Event.dow == 0: Monday, - 6: Sunday
        // java.time.dayOfWeek 1: MONDAY - 7: SUNDAY
        int dow = stamp.getTime().getDayOfWeek().getValue() - 1;

        // now that we know dow and time, search in database if there is
        // this time falls inbetween star-end of an Event at the apropriate
        // week day
        sql = "select * from event where dow = ? and start_time <= ? and end_time >= ?";
        sql = "select ev.* " +
              "from event ev join event2room r2e on ev.id = r2e.event_id " +
              "where  ev.dow = ? " +
              "  and ev.start_time <= ? " +
              "  and ev.end_time >= ?" +
              "  and r2e.room_id = ?";

        pstmt = conn.prepareStatement(sql);
        pstmt.setInt(1, dow);
        pstmt.setInt(2, Integer.parseInt(time_str));
        pstmt.setInt(3, Integer.parseInt(time_str));
        pstmt.setInt(4, stamp.getLocationId());
        rs  = pstmt.executeQuery();

        // we expect 0-1 result records. if we find multiple records, we
        // cannot assign this stamp to an event
        Boolean found = false;
        while (rs.next()) {
            if (found) {
                String out = String.valueOf(ret) + ", " + rs.getInt("id");
                logger.error("Multiple Events found in the same time period.");
                logger.error("event_id: " + out);
                throw new Exception("Multiple Events found in the same time period.");
            }

            logger.debug("Event: " + rs.getString("name"));
            ret = rs.getInt("id");
            found = true;
        }

        return ret;
    }

    public static String timeColonRemove(String t) {
        return t.replace(":", "");
    }

    public static String timeColonAdd(String t) {
        String min  = t.substring(t.length()-2, t.length());
        String hour = t.substring(0, t.length()-2);

        if (t.length() == 3)
            hour = "0" + hour;

        return hour + ":" + min;
    }
}
