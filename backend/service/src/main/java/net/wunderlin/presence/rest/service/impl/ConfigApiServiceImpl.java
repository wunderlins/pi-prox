package net.wunderlin.presence.rest.service.impl;

import net.wunderlin.presence.rest.service.*;
import net.wunderlin.presence.ConfigService;
import net.wunderlin.presence.DaoService;
import net.wunderlin.presence.rest.model.BackendError;
import net.wunderlin.presence.rest.model.Config;

import java.util.Date;
import java.util.Properties;

import net.wunderlin.presence.rest.service.NotFoundException;

import java.io.File;
import java.io.FileInputStream;
import java.time.OffsetDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class ConfigApiServiceImpl extends ConfigApiService {

    static Logger logger = LoggerFactory.getLogger(ConfigApiServiceImpl.class);
    protected static DaoService daoService = DaoService.getInstance();
    protected static ConfigService configService = ConfigService.getInstance();

    @Override
    public Response configGet(SecurityContext securityContext) throws NotFoundException {
        Config c = new Config();
        c.configFile(ConfigService.propertiesFile);

        try {
            c.port(configService.getConfig().getProperty("listen_port"));
            c.dbfile(daoService.getDbFileName());
        } catch (Exception e) {
            logger.error("config file path: " + ConfigService.propertiesFile);
            logger.error(e.getMessage());
            OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
            BackendError err = new BackendError()
                .code(2)
                .message(e.toString())
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        // add build version, this is take from pom.xml/project/version
        c.version(getClass().getPackage().getImplementationVersion());
        if (c.getVersion() == null) {
            Properties prop = new Properties();
            try {
                File f = new File(ConfigService.propertiesFolder + "/../../META-INF/maven/net.wunderlin/presence/pom.properties");
                if (f.exists()) {
                    prop.load(new FileInputStream(f));
                    String version = prop.getProperty("version");
                    c.version(version);
                }
                logger.info("exists: " + f.exists());
            } catch (Exception e) {
                logger.error(e.toString());
            }
        }

        // add application customizing
        c.setApplicationTitle(configService.getConfig().getProperty("gui_application_title", ""));
        c.setApplicationInfo(configService.getConfig().getProperty("gui_application_info", ""));

        return Response.ok().entity(c).build();
    }
}
