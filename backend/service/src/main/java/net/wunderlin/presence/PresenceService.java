package net.wunderlin.presence;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.wunderlin.presence.rest.model.StampRecord;

/**
 * general api for the rest service
 */
public class PresenceService {
    static Logger logger = LoggerFactory.getLogger(PresenceService.class);
    protected static DaoService daoService = DaoService.getInstance();
    protected static ConfigService configService = ConfigService.getInstance();
    protected static EventService eventService = EventService.getInstance();

    /**
     * singleton instance
     */
    private static PresenceService INSTANCE;

    /**
     * singleton
     * @return mock service instance
     * @throws Exception
     */
    public static PresenceService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new PresenceService();
        }

        return INSTANCE;
    }

    public PresenceService() {}

    /**
     * open ldap connection
     *
     * This method uses the configuration values from application.properties
     * to open an ldap connection.
     *
     * @return DirContext on success, null on error
     */
    public DirContext ldapConnection() {
        Hashtable<String, Object> env = new Hashtable<String, Object>(11);
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

        String ldap_url    = configService.properties.getProperty("ldap_url");
        String ldap_binddn = configService.properties.getProperty("ldap_binddn");
        String ldap_pass   = configService.properties.getProperty("ldap_pass");
        logger.debug("ldapConnection(): ldap_url: " + ldap_url + ", binddn: " + ldap_binddn);

        env.put(Context.PROVIDER_URL, ldap_url);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, ldap_binddn);
        env.put(Context.SECURITY_CREDENTIALS, ldap_pass);

        DirContext ctx = null;
        try {
            // Create initial context
            ctx = new InitialDirContext(env);
        } catch (Exception e) {
            logger.error("Failed to connect to LDAP Server");
            logger.error(env.toString());
        }

        return ctx;
    }

    /**
     * find person name from badge id
     *
     * @param UID
     * @return
     * @throws Exception
     * @throws NamingException
     */
    public StampRecord getResultFromUid(String UID) throws Exception, NamingException {
        logger.debug("Searching name for UID: " + UID);
        String name = null;
        StampRecord ret = new StampRecord();
        DirContext ctx = ldapConnection();
        NamingEnumeration<SearchResult> answer = null;

        if (ctx == null) {
            logger.error("Failed to conenct to LDAP Server");
            throw new Exception("Failed to conenct to LDAP Server");
        }

        if (ctx != null) {
            // search parameters
            String ldap_basedn = configService.properties.getProperty("ldap_basedn");
            String ldap_filter = configService.properties.getProperty("ldap_filter");
            String searchFilter = ldap_filter.replace("{UID}", UID);

            // Perform search in the entire subtree.
            SearchControls ctl = new SearchControls();
            ctl.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // search entities
            try {
                answer = ctx.search(ldap_basedn, searchFilter, ctl);
            } catch (NamingException e) {
                logger.error("LDAP search failed, reason: " + e.getMessage());
                try {
                    ctx.close();
                } catch (NamingException e1) {
                    // ignore, we try to be nice and close connections whenever possible.
                    logger.warn("Failed to close LDAP connection, reason: " + e.getMessage());
                }

                throw(e);
            }

            // we are done, close connection
            try {
                if (answer != null)
                    ctx.close();
            } catch (NamingException e) {
                logger.error("Failed to clsoe LDAP connection, reason: " + e.getMessage());
                throw(e);
            }
        }

        // find the display name of the person
        int count = 0;
        while (answer != null && answer.hasMore()) {
            SearchResult sr = (SearchResult) answer.next();
            logger.debug(">>" + sr.getNameInNamespace());
            Attributes attr = sr.getAttributes();
            logger.debug("  " + attr.get("displayName").get(0));
            // there might be multiple results, we always use the first
            if (name == null) {
                ret.setName((String) attr.get("displayName").get(0));
                ret.setsAMAccountName((String) attr.get("uniqueID").get(0));
            }
            // however, we keep counting for the logs
            count++;
        }

        // if we have not found a record, then try to resolve from imprivata data
        if (count == 0) {
            DaoService daoService = DaoService.getInstance();
            StampRecord r = daoService.getUserName(UID);

            if (r.getId() == 0) {
                ret.setName(r.getName());
                ret.setsAMAccountName(r.getsAMAccountName());
                count++;
            }
        }

        logger.info("LDAP Result for UID: " + UID + ", name: '" + ret.getName() + "', uniqeID: '"+ ret.getsAMAccountName() +"', num ofresults: " + count);
        if (count == 0) {
            logger.warn("Failed to resolve Legic UID in IDM: " + UID);
        }

        return ret;
    }

}
