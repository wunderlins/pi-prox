package net.wunderlin.presence.rest.service.factories;

import net.wunderlin.presence.rest.service.PingApiService;
import net.wunderlin.presence.rest.service.impl.PingApiServiceImpl;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class PingApiServiceFactory {
    private static final PingApiService service = new PingApiServiceImpl();

    public static PingApiService getPingApi() {
        return service;
    }
}
