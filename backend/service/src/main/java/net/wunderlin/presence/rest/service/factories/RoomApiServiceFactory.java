package net.wunderlin.presence.rest.service.factories;

import net.wunderlin.presence.rest.service.RoomApiService;
import net.wunderlin.presence.rest.service.impl.RoomApiServiceImpl;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class RoomApiServiceFactory {
    private static final RoomApiService service = new RoomApiServiceImpl();

    public static RoomApiService getRoomApi() {
        return service;
    }
}
