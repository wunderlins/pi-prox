package net.wunderlin.presence.rest.service.impl;

import net.wunderlin.presence.rest.service.*;
import net.wunderlin.presence.DaoService;
import net.wunderlin.presence.RoomService;
import net.wunderlin.presence.rest.model.BackendError;

import java.util.Date;
import net.wunderlin.presence.rest.model.Room;

import net.wunderlin.presence.rest.service.NotFoundException;

import java.time.OffsetDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.validation.constraints.*;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class RoomApiServiceImpl extends RoomApiService {
    static Logger logger = LoggerFactory.getLogger(RoomApiServiceImpl.class);
    public RoomService rs = new RoomService();

    @Override
    public Response deleteRoom(@Min(0) Integer id, SecurityContext securityContext) throws NotFoundException {
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        logger.info("Deleting Room: " + id.toString());
        Room room = rs.get(id);

        // 400, failed to fetch
        if (room == null) {
            logger.error("Failed to delete Room, could not fetch Room");
            BackendError err = new BackendError()
                .code(15)
                .message("Failed to delete Room, could not fetch Room")
                .ts(now);
                return Response.status(400).entity(err).build();
        }

        // 404, not found
        if (room.getId() == 0) {
            logger.error("Failed to delete Room, not found");
            BackendError err = new BackendError()
                .code(14)
                .message("Failed to delete Room, not found")
                .ts(now);
                return Response.status(404).entity(err).build();
        }

        // delete
        boolean ret = false;

        try {
            ret = rs.delete(room);
        } catch (Exception e) {
            String msg = "Cannot delete Room, still in use in the following Events: " + e.getMessage();
            logger.error(msg);
            BackendError err = new BackendError()
                .code(19)
                .message(msg)
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        // 500, failed to delete
        if (ret == false) {
            logger.error("Failed to delete Room");
            BackendError err = new BackendError()
                .code(14)
                .message("Failed to delete Room")
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        // return success
        return Response.ok().build();

    }

    @Override
    public Response updateRoom(@Min(0) Integer id, Room room, SecurityContext securityContext)
            throws NotFoundException {
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        room.setId(id); // url parameter always overrides data

        Room r = rs.get(id);

        // 400, failed to fetch
        if (r == null) {
            logger.error("Failed to update Room, could not fetch Room");
            BackendError err = new BackendError()
                .code(16)
                .message("Failed to update Room, could not fetch Room")
                .ts(now);
                return Response.status(400).entity(err).build();
        }

        // 404, not found
        if (r.getId() == 0) {
            logger.error("Failed to update Room, not found");
            BackendError err = new BackendError()
                .code(17)
                .message("Failed to update Room, not found")
                .ts(now);
                return Response.status(404).entity(err).build();
        }

        boolean ret = rs.update(room);
        if (ret == false) {
            logger.error("Failed to update Room");
            BackendError err = new BackendError()
                .code(18)
                .message("Failed to update Room")
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        // return success
        return Response.ok().entity(room).build();
    }

    @Override
    public Response insertRoom(Room room, SecurityContext securityContext) throws NotFoundException {
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);

        room = rs.insert(room);
        if (room == null) {
            logger.error("Failed to save Room");
            BackendError err = new BackendError()
                .code(12)
                .message("Failed to save Room")
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        // return success
        return Response.ok().entity(room).build();

    }

    @Override
    public Response getRoom(@Min(0) Integer id, SecurityContext securityContext) throws NotFoundException {
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        logger.info("Getting Room: " + id.toString());
        Room room = rs.get(id);

        // 400, failed to fetch
        if (room == null) {
            logger.error("Failed to fetch Room, could not fetch Room");
            BackendError err = new BackendError()
                .code(15)
                .message("Failed to fetch Room, could not fetch Room")
                .ts(now);
                return Response.status(400).entity(err).build();
        }

        // 404, not found
        if (room.getId() == 0) {
            logger.error("Failed to fetch Room, not found");
            BackendError err = new BackendError()
                .code(14)
                .message("Failed to fetch Room, not found")
                .ts(now);
                return Response.status(404).entity(err).build();
        }

        // return success
        return Response.ok().entity(room).build();
    }

}
