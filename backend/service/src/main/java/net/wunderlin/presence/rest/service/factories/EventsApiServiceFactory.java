package net.wunderlin.presence.rest.service.factories;

import net.wunderlin.presence.rest.service.EventsApiService;
import net.wunderlin.presence.rest.service.impl.EventsApiServiceImpl;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class EventsApiServiceFactory {
    private static final EventsApiService service = new EventsApiServiceImpl();

    public static EventsApiService getEventsApi() {
        return service;
    }
}
