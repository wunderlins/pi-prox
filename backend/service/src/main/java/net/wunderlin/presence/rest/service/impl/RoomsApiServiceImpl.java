package net.wunderlin.presence.rest.service.impl;

import net.wunderlin.presence.rest.service.*;
import net.wunderlin.presence.DaoService;
import net.wunderlin.presence.RoomService;
import net.wunderlin.presence.rest.model.BackendError;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import net.wunderlin.presence.rest.model.Room;

import net.wunderlin.presence.rest.service.NotFoundException;

import java.sql.SQLException;
import java.time.OffsetDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class RoomsApiServiceImpl extends RoomsApiService {
    static Logger logger = LoggerFactory.getLogger(RoomsApiServiceImpl.class);
    public RoomService rs = new RoomService();

    @Override
    public Response insertRooms(List<Room> rooms, SecurityContext securityContext) throws NotFoundException {
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        //List<Room> rooms = new ArrayList<Room>();

        boolean ret = rs.saveAll(rooms);
        if (!ret) {
            logger.error("Failed to save Rooms");
            BackendError err = new BackendError()
                .code(11)
                .message("Failed to save Rooms")
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        // return success
        return Response.status(201).build();
    }

    @Override
    public Response listRooms(SecurityContext securityContext) throws NotFoundException {
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        List<Room> rooms = new ArrayList<Room>();

        try {
            rooms = rs.getAll();

        } catch (SQLException e1) {
            logger.error(e1.getMessage());
            BackendError err = new BackendError()
                .code(11)
                .message(e1.toString())
                .ts(now);
            return Response.status(500).entity(err).build();
        }

        return Response.ok().entity(rooms).build();
    }
}
