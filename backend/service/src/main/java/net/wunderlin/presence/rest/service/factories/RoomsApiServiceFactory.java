package net.wunderlin.presence.rest.service.factories;

import net.wunderlin.presence.rest.service.RoomsApiService;
import net.wunderlin.presence.rest.service.impl.RoomsApiServiceImpl;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class RoomsApiServiceFactory {
    private static final RoomsApiService service = new RoomsApiServiceImpl();

    public static RoomsApiService getRoomsApi() {
        return service;
    }
}
