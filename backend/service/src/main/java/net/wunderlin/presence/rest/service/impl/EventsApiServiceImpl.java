package net.wunderlin.presence.rest.service.impl;

import net.wunderlin.presence.rest.service.*;
import net.wunderlin.presence.DaoService;
import net.wunderlin.presence.EventService;
import net.wunderlin.presence.rest.model.BackendError;
import net.wunderlin.presence.rest.model.Event;

import java.util.Date;
import java.util.List;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.sql.SQLException;
import java.time.OffsetDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class EventsApiServiceImpl extends EventsApiService {
    static Logger logger = LoggerFactory.getLogger(EventsApiServiceImpl.class);
    public EventService es = new EventService();

    @Override
    public Response listEvents(SecurityContext securityContext) throws NotFoundException {
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        List<Event> events = null;

        try {
            events = es.getAll();
        } catch (SQLException e1) {
            logger.error(e1.getMessage());
            BackendError err = new BackendError()
                .code(21)
                .message(e1.toString())
                .ts(now);
                return Response.status(500).entity(err).build();
        }

        return Response.ok().entity(events).build();
    }
}
