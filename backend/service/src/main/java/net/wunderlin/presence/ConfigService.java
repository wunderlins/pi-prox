package net.wunderlin.presence;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigService {

    static Logger logger = LoggerFactory.getLogger(ConfigService.class);
    //protected static DaoService daoService = DaoService.getInstance();

    public static String propertiesFile = "";
    public static String propertiesFolder = "";
    Properties properties = new Properties();
    boolean properties_loaded = false;

    /**
     * singleton instance
     */
    private static ConfigService INSTANCE;

    public ConfigService() {}

    /**
     * singleton
     * @return mock service instance
     * @throws Exception
     */
    public static ConfigService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new ConfigService();
            try {
                INSTANCE.loadConfigFile();
            } catch (Exception e) {
                return null;
            }
        }

        return INSTANCE;
    }

    /**
     * find configuration file
     *
     * In case of war deployement, we check the base folder of these classes (beginning at .net.wunderlin...)
     * In case of jar deployment, we use the default location (root of jar!application.properties).
     * But we also check in the same folder as the jar is deployed. This is the most convenient place to put
     * a system specific configuration for the application administrator.
     * @return
     * @throws Exception
     */
    protected String propertiesFile() throws Exception {
        if (propertiesFile != "")
            return propertiesFile;

        // configuration file detection for uber-jar
        String basePath = getClass().getResource("").toString();
        propertiesFile = basePath;
        logger.debug("basePath: " + basePath);

        // check if we have a jar
        if (basePath.substring(0, 4).equals("jar:")) {
            logger.debug("Reading properties from jar: url ...");
            String filePath = basePath.substring(0, basePath.indexOf("!")+1) + "application.properties";
            //System.out.println(filePath);
            propertiesFile = filePath;

            // check if there is a file on the same directory as the jar file.
            String outsideJar = filePath.substring(0, filePath.lastIndexOf("/")) + "/application.properties";
            outsideJar = outsideJar.replaceFirst("^jar:", "");
            outsideJar = outsideJar.replaceFirst("^file:", "");
            logger.debug("Checking for standalone config: " + outsideJar);
            File outsideJarFile = new File(outsideJar);
            if (outsideJarFile.exists()) {
                logger.debug("Found properties file next to jar at: " + outsideJar);
                propertiesFile = outsideJar;
            }
        } else if (basePath.substring(0, 5).equals("file:")) { // we are running from a war file or unit tests
            logger.debug("Reading properties from file: url ...");
            propertiesFile = basePath.substring(5);
            propertiesFile = propertiesFile.replaceAll("/net/wunderlin/.*$", ""); //, "/classes/application.properties");
            propertiesFile = propertiesFile.replaceAll("/test-classes.*$", "");
            propertiesFile += "/application.properties";
            //propertiesFile = basePath.substring(5, basePath.length()-32) + "classes/application.properties";
        } else {
            logger.error("application.properties not found!");
            throw new Exception("application.properties not found!");
        }

        propertiesFile = URLDecoder.decode(propertiesFile, "UTF-8");

        // if the 3rd character is a ":", then get rid of leading "/"
        if (propertiesFile.indexOf(":") == 2) {
            propertiesFile = propertiesFile.substring(1);
        }

        propertiesFolder = propertiesFile.substring(0, propertiesFile.lastIndexOf("/"));
        logger.info("using: " + propertiesFile);
        return propertiesFile;
    }

    /**
     * open configuration file
     * @throws Exception
     */
    public void loadConfigFile() throws Exception {
        if(properties_loaded) {
            logger.debug("loadConfigFile() already loaded, skipping");
            return;
        }

        logger.debug("loadConfigFile(): " + propertiesFile());
        String path = propertiesFile();
        InputStream in = null;
        if (path.substring(0, 4).equals("jar:")) {
            String resPath = "/" + path.substring(path.indexOf("!")+1);
            logger.debug("inside jar path: " + resPath);
            in = getClass().getResourceAsStream(resPath);
        } else {
            logger.debug("outside jar path: " + path);
            in = new FileInputStream(new File(path));
        }

        properties.load(in);
        logger.debug(properties.toString());
        properties_loaded = true;
    }

    /**
     * get all properties from application.properties
     * @return
     */
    public Properties getConfig() {
        return properties;
    }
}
