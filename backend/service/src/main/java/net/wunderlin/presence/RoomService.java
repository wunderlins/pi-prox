package net.wunderlin.presence;

import net.wunderlin.presence.rest.model.*;

import java.util.ArrayList;
import java.util.List;
import net.wunderlin.presence.rest.model.Room;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoomService {
    static Logger logger = LoggerFactory.getLogger(RoomService.class);
    protected static DaoService daoService = DaoService.getInstance();
    protected static PresenceService presenceService = PresenceService.getInstance();
    protected static EventService eventService = EventService.getInstance();

    /**
     * singleton instance
     */
    private static RoomService INSTANCE;

    /**
     * singleton
     * @return mock service instance
     * @throws Exception
     */
    public static RoomService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new RoomService();
        }

        return INSTANCE;
    }

    public Room getByDevice(String deviceName) {
        ResultSet rs = null;
        Room r = new Room();

        Connection conn = daoService.getConnection();
        String sql = "SELECT * FROM room WHERE device=?";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, deviceName);
            rs = pstmt.executeQuery();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

        try {
            if (rs.next()) {
                return new Room()
                    .id(rs.getInt("id"))
                    .deviceName(rs.getString("device"))
                    .roomName(rs.getString("room"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

        return r;
    }

    public List<Room> getAll() throws SQLException {
        List<Room> rooms = new ArrayList<Room>();
        Connection conn = daoService.getConnection();
        logger.debug("Getting all Rooms");
        String sql = "select * from room order by device";
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        pstmt = conn.prepareStatement(sql);
        rs = pstmt.executeQuery();

        while (rs.next()) {
            Room s;
            s = new Room()
                .id(rs.getInt("id"))
                .deviceName(rs.getString("device"))
                .roomName(rs.getString("room"));
            rooms.add(s);
        }

        return rooms;
    }

    public boolean saveAll(List<Room> rooms) {

        // id 0==new, but how do we know what to delete ?
        // -> we dont! use this.deletE(id) instead
        
        for (Room r: rooms) {
            boolean ret = true;
            if (r.getId() < 1) {
                Room rr = this.insert(r);
                if (rr == null)
                    ret = false;
            } else {
                ret = this.update(r);
            }

            if (!ret)
                return false;
        }

        return true;
    }

    public Room get(Integer id) {
        ResultSet rs = null;
        Room r = new Room();

        Connection conn = daoService.getConnection();
        String sql = "SELECT * FROM room WHERE id=?";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

        try {
            if (rs.next()) {
                return new Room()
                    .id(rs.getInt("id"))
                    .deviceName(rs.getString("device"))
                    .roomName(rs.getString("room"));
            } else {
                return r;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

    }

    public boolean update(Room room) {
        Connection conn = daoService.getConnection();
        String sql = "update room set room=?, device=? where id=?";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, room.getRoomName());
            pstmt.setString(2, room.getDeviceName());
            pstmt.setInt(3, room.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        }

        return true;
    }

    public Room insert(Room room) {
        Connection conn = daoService.getConnection();

        String sql = "insert into room (room, device) VALUES (?, ?)";

        // insert record
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, room.getRoomName());
            pstmt.setString(2, room.getDeviceName());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

        // get new id
        try {
            int last_id = -1;
            sql = "SELECT MAX(id) as c FROM room";
            Statement stmt_id  = conn.createStatement();
            ResultSet rs_id    = stmt_id.executeQuery(sql);

            // loop through the result set
            while (rs_id.next()) {
                last_id = rs_id.getInt("c");
            }
            room.setId(last_id);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

        return room;
    }

    public boolean delete(Room room) throws Exception {
        Connection conn = daoService.getConnection();

        String sql = "DELETE FROM room WHERE id=?";

        // check if this room is still used by some event
        List<Event> evs = getEvents(room);
        if (evs.size() > 0) {
            String events = "";
            for (Event ev: evs) {
                events += ev.getName() + ", ";
            }

            throw new Exception(events);
        }

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, room.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        }

        // make sure to remove all romm relations
        try {
            sql = "DELETE FROM event2room WHERE room_id=?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, room.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        }

        return true;
    }

    public List<Event> getEvents(Room room) {
        ResultSet rs = null;
        Connection conn = daoService.getConnection();
        List<Event> ev = new ArrayList<Event>();
        String sql = "select event_id from event2room where room_id = ?";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, room.getId());
            rs = pstmt.executeQuery();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }

        try {
            while (rs.next()) {
                ev.add(eventService.get(rs.getInt("event_id"), false));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return null;
        }


        return ev;
    }
}
