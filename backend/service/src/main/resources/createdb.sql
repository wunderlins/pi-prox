CREATE TABLE presence (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    stamp DATE,
    uid TEXT,
    device TEXT,
    location TEXT,
    location_id INTEGER DEFAULT -1,
    person TEXT,
    samaccountname TEXT,
    event TEXT,
    event_id INTEGER DEFAULT -1
);
CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE config (
    key TEXT UNIQUE,
    value TEXT
);
CREATE TABLE room (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    device TEXT  UNIQUE,
    room TEXT
);
CREATE TABLE IF NOT EXISTS "imprivata" (
    "LegicUID"      TEXT,
    "sAMAccountNAme"        TEXT,
    "GUID"  TEXT,
    "NedapID"       INTEGER,
    "PersonalId"    INTEGER,
    "EMail" TEXT,
    "FullName"      TEXT
);
CREATE TABLE event (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    start_time INTEGER, -- time format: `[H]HMM`
    end_time INTEGER,    -- time format: `[H]HMM`
    dow INTEGER, -- day of week 0 (mon) - 6 (Sun)
    deleted INTEGER DEFAULT 0 -- 0: false, 1: true
);
CREATE TABLE event2room (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    room_id INTEGER,
    event_id INTEGER
);
