package net.wunderlin.presence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.wunderlin.presence.rest.model.Room;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

public class TestRoom {
    static Logger logger = LoggerFactory.getLogger(TestRoom.class);
    //protected static PresenceService presenceService = PresenceService.getInstance();
    protected static DaoService daoService = DaoService.getInstance();
    public RoomService roomApi = new RoomService();
    static LocalDateTime dateTime = null;

    /**
     * @throws Exception
     */
    @BeforeAll
    static void beforeAll() throws Exception {
        dateTime = LocalDateTime.parse("2018-05-05T11:50:55");
    }

    @AfterEach
    void afterEach() {
        //System.out.println("After each test method");
    }

    @AfterAll
    static void afterAll() {
        //System.out.println("After all test methods");
    }

    /**
     * check if memory database was loaded successfully
     * @throws SQLException
     */
    @Test
    void TestRoom0() throws SQLException {
      logger.info("Testing Room API");
      // create a new Room
      Room r1 = new Room().roomName("R1").deviceName("D1");
      Room ret = this.roomApi.insert(r1);
      assertNotEquals(null, ret); // returns null on error
      assertNotEquals(0, ret.getId()); // default id is 0

      // get all rooms and find new room
      List<Room> rooms = this.roomApi.getAll();
      Room r = null;
      for(Room ir: rooms) {
        if (ir.getId() == ret.getId()) {
          r = ir;
          break;
        }
      }
      assertEquals(ret, r);

      // update room
      r1.setDeviceName("D1.1");
      boolean update_ret = this.roomApi.update(r1);
      assertEquals(true, update_ret);

      // fetch again and compare name
      Room updated = this.roomApi.get(ret.getId());
      assertEquals(r1.getDeviceName(), updated.getDeviceName());

      // delete Room again

      boolean delRoom = false;

      try {
        delRoom = this.roomApi.delete(r);
      } catch(Exception e) {;}
      assertEquals(true, delRoom);

      // get all rooms and find new room
      rooms = this.roomApi.getAll();
      r = null;
      for(Room ir: rooms) {
        if (ir.getId() == ret.getId()) {
          r = ir;
          break;
        }
      }
      assertEquals(null, r);

      // get room
      r = this.roomApi.get(ret.getId());
      assertEquals(0, r.getId());
    }

}
