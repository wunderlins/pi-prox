package net.wunderlin.presence;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.wunderlin.presence.rest.model.Event;
import net.wunderlin.presence.rest.model.Room;
import net.wunderlin.presence.rest.model.Stamp;
import net.wunderlin.presence.rest.model.StampRecord;
import net.wunderlin.presence.rest.service.impl.StampApiServiceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;


public class TestStamp {
    static Logger logger = LoggerFactory.getLogger(TestConfig.class);
    // protected static ConfigService configService = ConfigService.getInstance();
    // protected static DaoService daoService = DaoService.getInstance();
    protected static RoomService roomApi = RoomService.getInstance();
    protected static EventService eventApi = EventService.getInstance();
    protected static PresenceService presenceService = PresenceService.getInstance();

    static LocalDateTime dateTime = null;

    /**
     * Setup fixture
     * @throws Exception
     */
    @BeforeAll
    static void beforeAll() throws Exception {
        logger.info("Setting up TestStamp");
        dateTime = LocalDateTime.parse("2018-05-05T11:50:55");

        // cleanup, delete event and rooms
        List<Event> evs = eventApi.getAll();
        for (Event ev: evs) {
            if (ev.getName().equals("Event Inside") || ev.getName().equals("Event Outside")) {
                eventApi.purge(ev);
            }
        }
        List<Room> rms = roomApi.getAll();
        for (Room rm: rms) {
            if (rm.getDeviceName().equals("test-device-room-1") || rm.getDeviceName().equals("test-device-room-2")) {
                roomApi.delete(rm);
            }
        }
    }

    @Test
    void TestInit() {
        logger.info("TestInit()");
        // create 2 events.
        // - one that starts bevor the current hour and ends after
        // - the other that ends after the current hour
        // make sure dow matches today
        Room r1 = roomApi.insert(new Room().roomName("Test Room 1").deviceName("test-device-room-1"));
        Room r2 = roomApi.insert(new Room().roomName("Test Room 2").deviceName("test-device-room-2"));
        assertNotNull(r1);
        assertNotNull(r2);

        Integer H = Integer.valueOf(new Date().toInstant().atOffset(DaoService.zoneOffSet).format(DateTimeFormatter.ofPattern("HH")));
        logger.info("Hour: " + H);
        //Integer M = Integer.valueOf(OffsetDateTime.now().minusSeconds(5).format(DateTimeFormatter.ofPattern("mm")));
        String time_start_inside  = String.format("%01d", H) + ":00";
        String time_end_inside    = String.format("%01d", H+1) + ":00";
        String time_start_outside = String.format("%01d", H+1) + ":00";
        String time_end_outside   = String.format("%01d", H+2) + ":00";

        // map day of week. Event.dow == 0: Monday, - 6: Sunday
        // java.time.dayOfWeek 1: MONDAY - 7: SUNDAY
        int dow = OffsetDateTime.now().getDayOfWeek().getValue() - 1;

        List<Integer> rl1 = Arrays.asList(new Integer[]{r1.getId()});
        Event e_inside = eventApi.insert(new Event().name("Event Inside")
                                                    .start(time_start_inside)
                                                    .end(time_end_inside)
                                                    .dow(dow)
                                                    .roomIds(rl1));

        List<Integer> rl2 = Arrays.asList(new Integer[]{r2.getId()});
        Event e_outside = eventApi.insert(new Event().name("Event Outside")
                                                    .start(time_start_outside)
                                                    .end(time_end_outside)
                                                    .dow(dow)
                                                    .roomIds(rl2));
        assertNotNull(e_inside);
        assertNotNull(e_outside);
        logger.info("Created");
    }

    @Test
    void TestStampInsideRoomSlot() throws NamingException, Exception {
        logger.info("TestStampInsideRoomSlot()");

        // get inside event
        List<Event> evs = eventApi.getAll();
        Event evt = null;
        for (Event e: evs) {
            if (e.getName().equals("Event Inside") == true) {
                evt = e;
                break;
            }
        }
        assertNotNull(evt);

        // create new stamp
        int start_hour = Integer.parseInt(evt.getStart().replace(":00", ""));
        logger.info("Start Hour: " + start_hour);
        logger.info("Room: " + evt.getRoomIds());
        Room room = roomApi.get(evt.getRoomIds().get(0));
        logger.info("Room: " + room.getRoomName());

        // create a test stamp, it should be inside the sleected time range
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        Stamp stmp = new Stamp().location(room.getDeviceName()).stamp(now).uid("40E490D2");
        StampApiServiceImpl stampSvc = new StampApiServiceImpl();
        StampRecord stmpRec = stampSvc.insertStamp(stmp);
        logger.info(stmpRec.toString());

        // test if the appropriate event name is assoicated
        assertEquals("Event Inside", stmpRec.getEvent());
        assertNotEquals(-1, stmpRec.getLocationId());
        assertNotEquals(-1, stmpRec.getEventId());
        assertEquals(room.getId(), stmpRec.getLocationId());
        assertEquals(evt.getId(), stmpRec.getEventId());

    }

    @Test
    void TestStampOutsideRoomSlot() throws NamingException, Exception {
        logger.info("TestStampOutsideRoomSlot()");

        // get inside event
        List<Event> evs = eventApi.getAll();
        Event evt = null;
        for (Event e: evs) {
            if (e.getName().equals("Event Outside") == true) {
                evt = e;
                break;
            }
        }
        assertNotNull(evt);

        // create new stamp
        int start_hour = Integer.parseInt(evt.getStart().replace(":00", "")) + 1;
        logger.info("Start Hour: " + start_hour);
        logger.info("Room: " + evt.getRoomIds());
        Room room = roomApi.get(evt.getRoomIds().get(0));
        logger.info("Room: " + room.getRoomName());

        // create a test stamp, it should be inside the sleected time range
        OffsetDateTime now = new Date().toInstant().atOffset(DaoService.zoneOffSet);
        OffsetDateTime future = now.plusHours(1);
        Stamp stmp = new Stamp().location(room.getDeviceName()).stamp(future).uid("40E490D2");
        StampApiServiceImpl stampSvc = new StampApiServiceImpl();
        StampRecord stmpRec = stampSvc.insertStamp(stmp);
        logger.info(stmpRec.toString());

        // test if the appropriate event name is assoicated
        assertEquals("Event Outside", stmpRec.getEvent());
        assertNotEquals(-1, stmpRec.getLocationId());
        assertNotEquals(-1, stmpRec.getEventId());
        assertEquals(room.getId(), stmpRec.getLocationId());
        assertEquals(evt.getId(), stmpRec.getEventId());
    }
}
