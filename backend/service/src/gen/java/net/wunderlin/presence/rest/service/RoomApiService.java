package net.wunderlin.presence.rest.service;

import net.wunderlin.presence.rest.service.*;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import net.wunderlin.presence.rest.model.BackendError;
import net.wunderlin.presence.rest.model.Room;

import java.util.List;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.validation.constraints.*;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public abstract class RoomApiService {
    public abstract Response deleteRoom( @Min(0)Integer id,SecurityContext securityContext) throws NotFoundException;
    public abstract Response getRoom( @Min(0)Integer id,SecurityContext securityContext) throws NotFoundException;
    public abstract Response insertRoom(Room room,SecurityContext securityContext) throws NotFoundException;
    public abstract Response updateRoom( @Min(0)Integer id,Room room,SecurityContext securityContext) throws NotFoundException;
}
