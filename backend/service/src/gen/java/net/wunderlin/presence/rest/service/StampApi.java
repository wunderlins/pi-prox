package net.wunderlin.presence.rest.service;

import net.wunderlin.presence.rest.service.StampApiService;
import net.wunderlin.presence.rest.service.factories.StampApiServiceFactory;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import net.wunderlin.presence.rest.model.BackendError;
import java.io.File;
import java.time.LocalDate;
import net.wunderlin.presence.rest.model.Stamp;
import net.wunderlin.presence.rest.model.StampRecord;

import java.util.Map;
import java.util.List;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/stamp")


@io.swagger.annotations.Api(description = "the stamp API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class StampApi  {
   private final StampApiService delegate;

   public StampApi(@Context ServletConfig servletContext) {
      StampApiService delegate = null;

      if (servletContext != null) {
         String implClass = servletContext.getInitParameter("StampApi.implementation");
         if (implClass != null && !"".equals(implClass.trim())) {
            try {
               delegate = (StampApiService) Class.forName(implClass).newInstance();
            } catch (Exception e) {
               throw new RuntimeException(e);
            }
         }
      }

      if (delegate == null) {
         delegate = StampApiServiceFactory.getStampApi();
      }

      this.delegate = delegate;
   }

    @javax.ws.rs.POST
    
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "add a new stamp to the database", notes = "Add a stamp with badge id and device name to the database ", response = StampRecord.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "item created", response = StampRecord.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response insert(@ApiParam(value = "Inventory item to add") @Valid  Stamp stamp,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.insert(stamp, securityContext);
    }
    @javax.ws.rs.GET
    @Path("/csv")
    
    @Produces({ "text/csv", "application/json" })
    @io.swagger.annotations.ApiOperation(value = "", notes = "List logged in csv format", response = File.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "list of stamps from that day", response = File.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response listCsv(@ApiParam(value = "day to list", required = true) @QueryParam("day") @NotNull  LocalDate day,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.listCsv(day, securityContext);
    }
    @javax.ws.rs.GET
    
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "", notes = "List logged stamps", response = Stamp.class, responseContainer = "List", tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "list of stamps from that day", response = Stamp.class, responseContainer = "List"),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response listJson(@ApiParam(value = "day to list", required = true) @QueryParam("day") @NotNull  LocalDate day,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.listJson(day, securityContext);
    }
}
