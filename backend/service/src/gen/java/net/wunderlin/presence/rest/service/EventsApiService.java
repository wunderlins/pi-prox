package net.wunderlin.presence.rest.service;

import net.wunderlin.presence.rest.service.*;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import net.wunderlin.presence.rest.model.BackendError;
import net.wunderlin.presence.rest.model.Event;

import java.util.List;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.validation.constraints.*;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public abstract class EventsApiService {
    public abstract Response listEvents(SecurityContext securityContext) throws NotFoundException;
}
