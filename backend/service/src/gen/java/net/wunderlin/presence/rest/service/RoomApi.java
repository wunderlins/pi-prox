package net.wunderlin.presence.rest.service;

import net.wunderlin.presence.rest.service.RoomApiService;
import net.wunderlin.presence.rest.service.factories.RoomApiServiceFactory;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import net.wunderlin.presence.rest.model.BackendError;
import net.wunderlin.presence.rest.model.Room;

import java.util.Map;
import java.util.List;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/room")


@io.swagger.annotations.Api(description = "the room API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class RoomApi  {
   private final RoomApiService delegate;

   public RoomApi(@Context ServletConfig servletContext) {
      RoomApiService delegate = null;

      if (servletContext != null) {
         String implClass = servletContext.getInitParameter("RoomApi.implementation");
         if (implClass != null && !"".equals(implClass.trim())) {
            try {
               delegate = (RoomApiService) Class.forName(implClass).newInstance();
            } catch (Exception e) {
               throw new RuntimeException(e);
            }
         }
      }

      if (delegate == null) {
         delegate = RoomApiServiceFactory.getRoomApi();
      }

      this.delegate = delegate;
   }

    @javax.ws.rs.DELETE
    @Path("/{id}")
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Delete room", notes = "", response = Void.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "room deleted", response = Void.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 404, message = "not found", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response deleteRoom(@ApiParam(value = "Numeric ID of the room to delete", required = true, defaultValue = "0") @PathParam("id") @NotNull  @Min(0) Integer id,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.deleteRoom(id, securityContext);
    }
    @javax.ws.rs.GET
    @Path("/{id}")
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Get room", notes = "", response = Void.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "item get", response = Void.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 404, message = "not found", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response getRoom(@ApiParam(value = "Numeric ID of the room to get", required = true, defaultValue = "0") @PathParam("id") @NotNull  @Min(0) Integer id,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.getRoom(id, securityContext);
    }
    @javax.ws.rs.POST
    
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "mapping from devices to rooms", notes = "Create a new Room ", response = Room.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "items updated", response = Room.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response insertRoom(@ApiParam(value = "") @Valid  Room room,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.insertRoom(room, securityContext);
    }
    @javax.ws.rs.PUT
    @Path("/{id}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Update room", notes = "", response = Void.class, tags={ "developers", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "item updated", response = Void.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "bad input parameter", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 404, message = "not found", response = BackendError.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "server error", response = BackendError.class)
    })
    public Response updateRoom(@ApiParam(value = "Numeric ID of the room to update", required = true, defaultValue = "0") @PathParam("id") @NotNull  @Min(0) Integer id,@ApiParam(value = "") @Valid  Room room,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.updateRoom(id, room, securityContext);
    }
}
