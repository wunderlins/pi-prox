package net.wunderlin.presence.rest.service;

import net.wunderlin.presence.rest.service.*;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import net.wunderlin.presence.rest.model.BackendError;
import java.io.File;
import java.time.LocalDate;
import net.wunderlin.presence.rest.model.Stamp;
import net.wunderlin.presence.rest.model.StampRecord;

import java.util.List;
import net.wunderlin.presence.rest.service.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.validation.constraints.*;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public abstract class StampApiService {
    public abstract Response insert(Stamp stamp,SecurityContext securityContext) throws NotFoundException;
    public abstract Response listCsv( @NotNull LocalDate day,SecurityContext securityContext) throws NotFoundException;
    public abstract Response listJson( @NotNull LocalDate day,SecurityContext securityContext) throws NotFoundException;
}
