# Setup Development Environment

- tomcat 9 for debugging (tomcat 10 not supported, still using javax packages)
- Install npm >= 8
- Install Java 11+ and mvn
- Install jq

```bash
cd backend
npm install
```

Then do an npm install in the gui directory as well:

```bash
cd gui
npm install
```


# Building a Release

All paths are relative to the projet root.

## 1. bump version number

The version number is taken from `backend/service/pom.xml`, change the
value in `//project/version/text()`:

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" 
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    ...
    <version>0.5.0</version>
    ...
</project>
```

The schema of the version number is 3 integers: `major.minor.patch`.

## 2. build angular release

```bash
cd backend/gui
ng build
# commit the new release fiels into the repo
git commit -am "new gui build"
```

This npm script will do the following:

- build an angula release version in `../gui-build` (temp folder)
- when successfull, remove all *.js files from the backend web root in `../service/src/main/webapp/ui/*.[js|css]`
- the copy the newly compiled build files to the backend service's web root: `cp -Rp ../gui-build/* ../service/src/main/webapp/ui/`

## 3. build backend jar

If you do not want to run unit and integration tests, then you may
build a release jar like so:

```bash
cd backend/service
mvn clean package assembly:single -DskipTests=true 
```

This will build 2 release files in `backend/service/target`:

- `presence.war`: normally used for debuggign, can be deployed to tomcat 9.x
- `presence-X.X.X-jar-with-dependencies.jar` (rename it to presence-X.X.X.jar). This is the self contained executable including apache tomcat 9.x. This file is normally deployed.

## 4. tag the release

Now that everything is build, tag the release like so

```bash
git tag BACKEND_X.X.X
git push --tags # push the tag to origin
```
