#!/usr/bin/env bash

OUTPUT_DIR=api
PACKAGE=net.wunderlin
ARTIFACT=presence

rm -rf $OUTPUT_DIR
#export JAVA_POST_PROCESS_FILE="/usr/bin/clang-format -i"

./openapi-generator-cli generate \
    -o $OUTPUT_DIR/jersey \
    -g jaxrs-jersey \
    -i ../service/src/main/webapp/openapi.yaml \
    -c jaxrs-config.yaml \
    --additional-properties java11=true

echo ""
echo "################################################################################"
echo "# finished generating                                                          #"
echo "#                                                                              #"
echo "# Deploying and Checking implementations ...                                   #"
echo "################################################################################"

# copy latest openapi spec into WEB-INF
#cp ../openapi/inventory_api.yaml ../src/main/webapp/WEB-INF/

# java model post processing
for m in $(ls api/jersey/src/gen/java/net/wunderlin/${ARTIFACT}/rest/model/*.java); do
    echo $m;
    #sed -i 's/private/protected/' $m
done
echo ""

# copy generated sources, implementation must be done manually
mkdir -p ../service/src/gen/ 2>/dev/null
rm -rf ../service/src/gen/*
cp -r api/jersey/src/gen/* ../service/src/gen/

# copy all implementation files that do not exist
cd api/jersey
dirs=$(find src/main/java -type d)
files=$(find src/main/java -type f)
cd ../../../service # back
for d in $dirs; do
    if [ ! -d $d ]; then
        echo $d
        mkdir -p $d 
    fi
done

for f in $files; do
    # do not overwrite
    if [ ! -e $f ]; then 
        echo $f
        cp -n ../openapi/api/jersey/$f $f
    fi
done

# check implementations, if we find "do some magic", some implementations 
# are missing
keyword="do some magic"
for f in $files; do
    grep "$keyword" "$f" >/dev/null 2>&1
    exitcode=$?
    if [[ $exitcode -eq 0 ]]; then
        echo -en "$(tput setaf 1)Missing implementation in: ${f##*/}$(tput sgr0)\n"
        num=$(grep --line-number --no-filename "$keyword" "$f" | cut -f1 -d':')
        for n in $num; do 
            prev=$(( $n - 1 ))
            echo -en "$prev: "
            sed -n ${prev}p "$f" | sed -e 's/ *//; s/) .*/)/'
        done
        echo ""
    fi
done
exit 0;
