#!/usr/bin/env bash

./openapi/openapi-generator-cli generate -i ./service/src/main/webapp/openapi.yaml -g typescript-angular -c ./openapi/angular-config.yaml -o ./gui/src/app/api/1.0.0 && ./fixLocalVarHeaders.sh gui/src/app/api/1.0.0/api/ && { cd openapi; ./generate.sh; }
