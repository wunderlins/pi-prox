import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse, HttpResponseBase} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ErrorDialog } from './error-dialog'
import { MatDialog } from '@angular/material/dialog';
import { ErroMessage } from './error-message'

/**
 * This class intercepts all http traffic
 *
 * It is possible to detect/modify requests and responses as well as
 * to implement global error handling.
 */
@Injectable()
export class NetInterceptor implements HttpInterceptor {
    constructor(public dialog: MatDialog) {}

    handleResponse(started: number, req: HttpRequest<any>, res: HttpResponseBase) {
        const elapsed = Date.now().valueOf() - started;
        // check if this is an error
        const error = (res instanceof HttpErrorResponse) ? true : false;
        // no error? just log the request and the response
        console.log(`${elapsed}, error: ${error}, ${req.method} ${req.url} ${res.status}`);

        // modal error dialog
        if (error) {
            const data: ErroMessage = {elapsed: started, req: req, res: res as HttpErrorResponse}
            const dialogRef = this.dialog.open(ErrorDialog, {
                width: '250px',
                data: data
            });

            dialogRef.afterClosed().subscribe((result: any) => {
                console.log('The dialog was closed');
            });
        }
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const started = Date.now().valueOf();
        // modify request here
        //console.log("requesting: " + req.url);

        return next.handle(req).pipe(
            map((event: HttpEvent<any>) => {
                // modify responses here
                if (event instanceof HttpResponse) {
                    this.handleResponse(started, req, event);
                    event = event.clone();
                }
                return event;
            }), catchError((error: HttpErrorResponse) => {
                // global http error handler.
                this.handleResponse(started, req, error);
                return throwError(error);
            }
        ));
    }
}
