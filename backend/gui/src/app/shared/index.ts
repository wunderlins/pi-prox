/* "Barrel" of Http Interceptors */
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { NetInterceptor } from './net-interceptor';

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: NetInterceptor, multi: true },
];

export * from './shared.module';
export * from './error-message';
export * from './error-dialog';
export * from './toast.component';
