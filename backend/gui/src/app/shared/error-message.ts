import { HttpErrorResponse, HttpRequest } from "@angular/common/http";

export interface ErroMessage {
  elapsed: number,
  req: HttpRequest<any>,
  res: HttpErrorResponse
};
