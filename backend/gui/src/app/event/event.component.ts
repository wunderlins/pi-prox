import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { formatDate } from '@angular/common';
import { SharedModule, ToastComponent } from '../shared/'
import { Room } from '../api/1.0.0/model/room'
import { Event as TEvent } from '../api/1.0.0/model/event'
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { MatAutocomplete, MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { UntypedFormGroup, FormControl,UntypedFormArray, UntypedFormBuilder, ValidatorFn, ValidationErrors, AbstractControl, Validators, FormGroupDirective, NgForm } from '@angular/forms'
import { MatFormField, MatFormFieldControl } from '@angular/material/form-field';
import { ErrorStateMatcher } from '@angular/material/core';
import { environment } from '../../environments/environment';

/*
declare module "@angular/forms/" {
    interface NgForm {
      resetValidation(this:Form) : void;
    }
}

NgForm.prototype.resetValidation = function(this:NgForm) {
    Object.keys(this.controls).forEach((key) => {
        const control = this.controls[key];
        control.markAsPristine();
        control.markAsUntouched();
    });
}
*/

// single field validator
export function timeValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    var format = /^[0-9]{2}:[0-9]{2}$/.test(control.value);

    /**
     * - split must produce 2 items, Hour:Minute
     * - hour in range of 0-23
     * - minute in range of 0-59
     */
    const hm = (control.value as String).split(":");
    const valid_time = hm.length == 2 &&
                       (parseInt(hm[0]) <= 23 && parseInt(hm[0]) >= 0) &&
                       (parseInt(hm[1]) <= 59 && parseInt(hm[1]) >= 0);

    return (!format || !valid_time) ? {format: {value: control.value}} : null;
  };
}

// formGroup validator
export const timeRangeValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  //console.log(control)
  const start = control.get('start');
  const end   = control.get('end');
  const dow   = control.get('dow');

  const hm_start = (start?.value as String).split(":");
  const hm_end   = (end?.value as String).split(":");

  var start_ts = {"h": -1, "m": -1, ts: -1 };
  if (hm_start.length == 2) {
    start_ts = {"h": parseInt(hm_start[0]), "m": parseInt(hm_start[1]), ts: 0 };
    start_ts.ts = start_ts.h * 100 + start_ts.m;
  }

  var end_ts = {"h": -1, "m": -1, ts: -1 };
  if (hm_end.length == 2) {
    end_ts = {"h": parseInt(hm_end[0]), "m": parseInt(hm_end[1]), ts: 0 };
    end_ts.ts = end_ts.h * 100 + end_ts.m;
  }

  const valid_time_start = start_ts.ts != -1 &&
                           (start_ts.h <= 24 && start_ts.h >= 0) &&
                           (start_ts.m <= 59 && start_ts.m >= 0);
  const valid_time_end   = end_ts.ts != -1 &&
                           (end_ts.h <= 24 && end_ts.h >= 0) &&
                           (end_ts.m <= 59 && end_ts.m >= 0);

  //return null;
  if (end_ts.ts <= start_ts.ts) {
    start?.setErrors({ timeRange: true });
    end?.setErrors({ timeRange: true });
    start?.markAsTouched();
    end?.markAsTouched();
    console.log({ timeRange: true });
  } else if (!valid_time_start || !valid_time_end) {
    start?.setErrors({ timeFormat: true });
    end?.setErrors({ timeFormat: true });
    start?.markAsTouched();
    end?.markAsTouched();
    console.log({ timeFormat: true });
  } else  {
    let new_start_errors = removeProperties(start?.errors, ['timeFormat', 'timeRange'])
    let new_end_errors   = removeProperties(end?.errors, ['timeFormat', 'timeRange'])

    start?.setErrors(new_start_errors);
    end?.setErrors(new_end_errors);

    //console.log(new_end_errors);
  }



  return null;
};

// https://stackabuse.com/how-to-filter-an-object-by-key-in-javascript/
function removeProperties(o: any, keys: Array<any>): any {
    if (o == null) return o;
    const ret = Object.keys(o)
    .filter((k) => !keys.includes(k))
    .reduce((obj, key) => {
        return Object.assign(obj, {
            [key]: o[key]
        });
    }, {});

    if (Object.keys(ret).length == 0)
      return null;

    return ret;
}

//export const minLengthArray = (min: number): ValidationErrors | null => {
export function minLengthArray(min: number): ValidatorFn {
  return (c: AbstractControl): {[key: string]: any} | null => {
    //console.log(c.value)
    if (c.value.length >= min)
      return null;

    return { inLengthArray: {valid: false } };
  }
}

/*
export class ArrayListErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    console.log(control?.value)
    return false;
  }
}
*/

/**
 * The use of mat-chip-list in a form arary is problematic
 * since it needs to address the popup list with a @“iewChild which (to my knowledge)
 * cannot have a dynamic name.
 *
 * So the solution is to make it it's own component:
 * https://stackoverflow.com/questions/53109833/angular-material-matchiplist-how-to-use-it-on-a-dynamic-formarray
 */

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit, OnDestroy {
  env = environment;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  private selectedInput: UntypedFormGroup | null = null;

  public rooms_loaded = false;
  public rooms = new Array<Room>();
  private room_subscr!: Subscription;
  private event_subscr!: Subscription;

  private roomsSubject = new BehaviorSubject<Room[]>([]);
  rooms$: Observable<Room[]> = this.roomsSubject.asObservable();

  //@ViewChild(MatAutocompleteTrigger, { read: MatAutocompleteTrigger })
  //matAutocomplete!: MatAutocompleteTrigger;

  public dowList =  [
    "Montag",
    "Dienstag",
    "Mittwoch",
    "Donnerstag",
    "Freitag",
    "Samstag",
    "Sonntag"
  ]

  // fb implemntation
  // https://www.tektutorialshub.com/angular/angular-formarray-example-in-reactive-forms/
  eventsForm: UntypedFormGroup;

  constructor(public shared: SharedModule,
              private toast: ToastComponent,
              private fb:UntypedFormBuilder ) {
    this.eventsForm = this.fb.group({
      events: this.fb.array([]) ,
    });
  }

  public get events() : UntypedFormArray {
    return this.eventsForm.get("events") as UntypedFormArray
  }

  newEvent(
    id: number = 0,
    name: string = '',
    start: string = '',
    end: string = '',
    dow: number = 0,
    room_ids: Array<number> = [],
    deleted: boolean = false
  ): UntypedFormGroup {
    let ret = this.fb.group({
      id: id,
      name: name,
      start: start,
      end: end,
      dow: dow,
      // must be array in array, because reasons!
      // https://stackoverflow.com/questions/37564574/angular-2-typescript-typeerror-this-validator-is-not-a-function
      room_ids: (room_ids) ? [room_ids] : [[]],
      deleted: deleted
    })
    if (deleted)
      ret.disable();

    ret.get('name')?.addValidators([Validators.required]);
    ret.get('start')?.addValidators([Validators.required, timeValidator()]);
    ret.get('end')?.addValidators([Validators.required, timeValidator()]);
    ret.get('dow')?.addValidators([Validators.required, Validators.min(0), Validators.max(6)]);
    //ret.get('room_ids')?.addValidators([Validators.required, minLengthArray(1)]);
    ret.addValidators([timeRangeValidator]);
    return ret;
  }

  addEvent(evt: UntypedFormGroup) {
    this.events.push(evt);
  }

  removeEvent(i: number) {
    let ev = this.events.at(i);
    ev.get("deleted")?.setValue(true);
    this.events.at(i).disable();
    this.save(i);
  }

  enableEvent(i: number) {
    let ev = this.events.at(i);
    ev.get("deleted")?.setValue(false);
    this.events.at(i).enable();
    this.save(i);
  }

  onSubmit() {
    console.log(this.eventsForm.value);
  }

  save(idx: number) {
    let form: UntypedFormGroup = this.eventsForm.get(['events', idx]) as UntypedFormGroup;
    let data: TEvent = form.value;
    let id: number = data.id ?? 0;
    console.log(data);

    if (id) {
      this.shared.service.updateEvent(id, data).subscribe(res => {
        // if save was successfull, set the form back to pristine
        form.markAsPristine();
      });
    } else {
      this.shared.service.insertEvent(data).subscribe(res => {
        // intercept the nw id and update the event store
        this.events.at(this.events.length-1).get("id")?.setValue(res.id);

        // if save was successfull, set the form back to pristine
        form.markAsPristine();
      });
    }
  }

  add() {
    this.addEvent(this.newEvent());
    this.roomChange([], this.events.length-1);
  }

  ngOnInit(): void {
    this.events.clear();
    this.event_subscr = this.shared.events$.subscribe(res => {
      var idx: number = 0;
      for(let e of res) {
        //console.log(e)
        this.addEvent(this.newEvent(
          e.id ?? undefined,
          e.name ?? undefined,
          e.start ?? undefined,
          e.end ?? undefined,
          e.dow ?? undefined,
          e.room_ids,
          e.deleted ?? false
        ));
      }
    });

    // get and cache rooms
    this.room_subscr = this.shared.rooms$.subscribe(res => {
      this.rooms = [];

      for(let r in res) {
        let room: Room = res[r];
        if (room) {
          this.rooms[room?.id ?? 0] = room;
          //this.rooms.push(room);
        }
      }

      //console.log(this.rooms);
      // get the events, now that we have the rooms
      //if (this.rooms.length)
      //console.log(res)
      //this.shared.getEvents();
      this.rooms_loaded = true;
    });

    //this.shared.getRooms();
    //this.shared.getEvents();
  }

  /*
  selectInput(ix: number, event: Event) {
    this.selectedInput = this.events.at(ix) as UntypedFormGroup;
    let ids = this.selectedInput.get('room_ids')?.value
    console.log(ids);

    // generate a new subject for the room drowpdown list
    var unselectedRooms: Room[] = <Room[]>([]);
    for (let i in this.rooms) {
      let ii = parseInt(i);
      if (!ids || (ids.indexOf(ii) == -1))
        unselectedRooms.push(this.rooms[ii])
    }

    //console.log(unselectedRooms)
    this.roomsSubject.next(unselectedRooms);

    // open the menu if it is closed
    if (event.type == "click") {
      // after selection the panel is close, this will re-open it
    }
  }
  */

  unSelectInput() {
    //this.selectedInput = null;
  }

  ngOnDestroy(): void {
    this.room_subscr.unsubscribe();
    this.event_subscr.unsubscribe();
  }

  /*
  add_room(event: MatChipInputEvent, ev: TEvent): void {
    const value = (event.value || '').trim();
    console.log(value)

    if (event.chipInput != undefined) {
      event.chipInput.inputElement.value = "";
    }
    event.chipInput!.clear();
  }
  */

  /*
  remove_room(rid: number, ix: number): void {
    if (!this.events.at(ix).get('room_ids')?.value) return;
    var new_rooms: Array<number> = [];
    var room_ids = this.events.at(ix).get('room_ids');
    for (let i of this.events.at(ix).get('room_ids')?.value ?? [])
      if (i !== rid)
        new_rooms.push(i);

    room_ids?.setValue(new_rooms);
    this.events.at(ix).markAsTouched();

    if (new_rooms.length == 0) {
      //room_ids?.setValue([]);
      room_ids?.setErrors({'required': true});
      //console.log(room_ids?.errors)
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (!this.selectedInput) {
      return;
    }
    var ev: UntypedFormGroup = this.selectedInput;

    console.log("Room: " + event.option.value.id)
    console.log("Event: " + ev.controls.id.value)

    if (!ev.controls.room_ids.value) ev.controls.room_ids.setValue([]);
    if (ev.controls.room_ids.value.indexOf(event.option.value.id) != -1) return;
    ev.controls.room_ids.value.push(event.option.value.id)

    event.source.panel.nativeElement.value = '';

    ev.controls.room_ids.setErrors(null);

    /*
    event.source.showPanel = true;
    ev.markAsDirty();
    ev.controls.room_ids.setErrors(null);
    * /
    //ev.controls.room_ids.errorState
  }
  */

  click() {
    console.log("click")
    // after selection the panel is close, this will re-open it
    //console.log(this.matAutocomplete.panelOpen)
    //if (!this.matAutocomplete.panelOpen)
    //  this.matAutocomplete.openPanel();

  }

  roomChange(rooms: number[], idx: number) {
    //console.log(rooms)

    this.events.at(idx).markAsDirty();
    if (rooms.length == 0) {
      let room_ids = this.events.at(idx).get('room_ids');
      if (!room_ids || !room_ids.value || room_ids.value.length == 0) {
        room_ids?.setErrors({'valid': false})
        room_ids?.markAllAsTouched();
      }
    } else {
      this.events.at(idx).get('room_ids')?.setErrors(null);
    }
  }
}
