import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDividerModule } from '@angular/material/divider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDialogModule } from '@angular/material/dialog';

import { SharedModule } from './shared/shared.module'

import { environment } from '../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StampComponent } from './stamp/stamp.component';

import {
  ApiModule,
  Configuration,
  ConfigurationParameters,
} from './api/1.0.0';
import { RoomComponent } from './room/room.component';
import { EventComponent } from './event/event.component';
import { RoomListComponent } from './event/room-list.component';

import { httpInterceptorProviders } from './shared'

export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {
    basePath: environment.backend_url,
  };
  return new Configuration(params);
}

const routes: Routes = [
  { path: '', redirectTo: '/stamp', pathMatch: 'full' },
  { path: 'stamp', component: StampComponent },
  { path: 'room', component: RoomComponent },
  { path: 'event', component: EventComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    StampComponent,
    RoomComponent,
    EventComponent,
    RoomListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonToggleModule,
    MatTableModule,
    MatMenuModule,
    MatSnackBarModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatDialogModule,

    BrowserModule,
    AppRoutingModule,
    ApiModule.forRoot(apiConfigFactory),
    RouterModule.forRoot(routes, {useHash: true})
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'de-CH'},
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent],
  exports: [SharedModule, BrowserModule, RouterModule],
  schemas: []
})
export class AppModule {
}
