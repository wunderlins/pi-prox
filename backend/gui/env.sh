# source this file to extend PATH variable for use with the
# project's nodo modules:
# $ . env.sh
export PATH=$PWD/node_modules/.bin:$PATH
source <(ng completion script)